import { USER_API } from "../common/errors";
import appConfig from "../config/application";
import ValidationError from "./error";

/**
 * User
 */
class User {

    /**
     * Constructor
     *
     * @param name User's name (Optional)
     * @param mail User's mail
     * @param password User's password (hash)
     * @param role User's role
     * @param auth Authentication information
     * @param workspacesLimit Workspaces limit
     */
    constructor(name, mail, password, role, auth, workspacesLimit = appConfig.workspacesLimit) {
        this.name = name;
        this.mail = mail;
        this.password = password;
        this.role = role;
        this.auth = auth;
        this.workspacesLimit = workspacesLimit;
    }

    /**
     * Get key
     *
     * @return {string} Key
     */
    get key() {
        return this._key;
    }

    /**
     * Set key
     *
     * @param key Key
     */
    set key(key) {
        this._key = key;
    }

    /**
     * Set name
     *
     * @param name Name
     */
    set name(name) {
        this._name = name;
    }

    /**
     * Get name
     *
     * @return {string} Name
     */
    get name() {
        return this._name;
    }

    /**
     * Set mail
     *
     * @param mail Mail
     */
    set mail(mail) {
        this._mail = mail;
    }

    /**
     * Get mail
     *
     * @return {string} Mail
     */
    get mail() {
        return this._mail;
    }

    /**
     * Set password
     *
     * @param password Password
     */
    set password(password) {
        this._password = password;
    }

    /**
     * Get password
     *
     * @return {string} Password
     */
    get password() {
        return this._password;
    }

    /**
     * Set role
     *
     * @param role Role
     */
    set role(role) {
        this._role = role;
    }

    /**
     * Get role
     *
     * @return {UserRole} Role
     */
    get role() {
        return this._role;
    }

    /**
     * Set authentication information
     *
     * @param auth Auth info
     */
    set auth(auth) {
        this._auth = auth;
    }

    /**
     * Get authentication information
     *
     * @return {AuthInfo} Authentication information
     */
    get auth() {
        return this._auth;
    }

    /**
     * Get workspaces limit
     *
     * @return {number} Limit
     */
    get workspacesLimit() {
        return this._workspacesLimit;
    }

    /**
     * Get workspaces limit
     * @param value
     */
    set workspacesLimit(value) {
        this._workspacesLimit = value;
    }

    /**
     * Convert object into json
     *
     * @return {{password: string, role: UserRole, mail: string, auth: {token: string}, name: string}} JSON
     */
    toJSON() {
        return {
            name: this.name,
            mail: this.mail,
            password: this.password,
            role: this.role,
            auth: this.auth ? this.auth.toJSON() : undefined,
            workspacesLimit: this.workspacesLimit
        };
    }

    /**
     * Convert an object into an User
     *
     * @param object Object
     * @return {User} User
     */
    static FromObject(object) {
        const user = new User();

        if (object instanceof Object) {
            if (object.name !== undefined) {
                user.name = object.name;
            }
            if (object.mail !== undefined) {
                user.mail = object.mail;
            }
            if (object.password !== undefined) {
                user.password = object.password;
            }
            if (object.role !== undefined) {
                user.role = object.role;
            }
            if (object.auth !== undefined) {
                user.auth = AuthInfo.FromObject(object.auth);
            }
            if (object.workspacesLimit !== undefined) {
                user.workspacesLimit = object.workspacesLimit;
            }
            if (object._key !== undefined) {
                user.key = object._key;
            }
        }

        return user;
    }

    /**
     * Check an user, throw an error if user isn't valid
     *
     * @param user User
     * @param mandatoryError Error when a mandatory field isn't defined
     */
    static Check(user, mandatoryError = true) {
        if (!(user instanceof User)) {
            throw new ValidationError(USER_API.INVALID_USER);
        }

        if (user.name !== undefined && typeof user.name !== "string") {
            throw new ValidationError(USER_API.INVALID_USER_NAME);
        }

        if (user.mail === undefined && mandatoryError) {
            throw new ValidationError(USER_API.USER_MAIL_MANDATORY);
        } else if (user.mail !== undefined && (typeof user.mail !== "string" || !User.MAIL_REGEX.test(user.mail))) {
            throw new ValidationError(USER_API.INVALID_USER_MAIL);
        }

        if (user.password === undefined && mandatoryError) {
            throw new ValidationError(USER_API.USER_PASSWORD_MANDATORY);
        } else if (user.password !== undefined && typeof user.password !== "string") {
            throw new ValidationError(USER_API.INVALID_USER_PASSWORD);
        }

        if (user.role === undefined && mandatoryError) {
            throw new ValidationError(USER_API.USER_ROLE_MANDATORY);
        } else if (user.role !== undefined) {
            UserRole.Check(user.role);
        }

        if (user.auth !== undefined) {
            AuthInfo.Check(user.auth, mandatoryError);
        }

        if (user.workspacesLimit === undefined) {
            throw new ValidationError(USER_API.WORKSPACE_LIMIT_MANDATORY);
        } else if (!Number.isInteger(user.workspacesLimit) || Number.isInteger(user.workspacesLimit) && user.workspacesLimit < 0) {
            throw new ValidationError(USER_API.INVALID_WORKSPACE_LIMIT);
        }
    }

    /**
     * Get mail regex
     *
     * @return {RegExp} Regex
     */
    static get MAIL_REGEX() {
        // eslint-disable-next-line no-useless-escape
        return new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    }

    static get MAIL_FIELD_NAME() {
        return "mail";
    }
}

/**
 * Authentication information
 */
class AuthInfo {

    /**
     * Constructor
     *
     * @param token Token
     */
    constructor(token) {
        this.token = token;
    }

    /**
     * Set token
     *
     * @param token Token
     */
    set token(token) {
        this._token = token;
    }

    /**
     * Get token
     *
     * @return {string} Token
     */
    get token() {
        return this._token;
    }

    /**
     * Convert object into json
     *
     * @return {{token: string}} JSON
     */
    toJSON() {
        return {
            token: this.token
        };
    }

    /**
     * Check authentication information, throw an error if info isn't valid
     *
     * @param auth Auth info
     * @param mandatoryError Error when a mandatory field isn't defined
     */
    static Check(auth, mandatoryError = true) {
        if (!(auth instanceof AuthInfo)) {
            throw new ValidationError(USER_API.INVALID_AUTH_INFO);
        }

        if (auth.token === undefined && mandatoryError) {
            throw new ValidationError(USER_API.AUTH_TOKEN_MANDATORY);
        } else if (auth.token !== undefined && typeof auth.token !== "string") {
            throw new ValidationError(USER_API.INVALID_AUTH_TOKEN);
        }
    }

    /**
     * Convert an object into a AuthInfo
     *
     * @param object Object
     * @return {AuthInfo} Auth info
     */
    static FromObject(object) {
        const info = new AuthInfo();

        if (object instanceof Object && object.token !== undefined) {
            info.token = object.token;
        }

        return info;
    }
}

/**
 * All user roles
 */
class UserRole {
    static get ADMIN() {
        return "ADMIN";
    }

    static get USER() {
        return "USER";
    }

    /**
     * Check role, throw an error if role isn't valid
     *
     * @param role Role
     */
    static Check(role) {
        if (role !== UserRole.ADMIN && role !== UserRole.USER) {
            throw new ValidationError(USER_API.INVALID_USER_ROLE);
        }
    }
}

export { User, AuthInfo, UserRole };