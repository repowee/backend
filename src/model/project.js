import { PROJECT_API } from "../common/errors";
import { UserCollectionService } from "../services/db/user";
import { WorkspaceCollectionService } from "../services/db/workspace";
import ValidationError from "./error";

/**
 * Project class
 */
class Project {

    /**
     * Constructor
     *
     * @param name Project's name
     * @param creator Creator
     * @param workspace Workspace
     * @param image Image
     * @param container Container
     * @param ports Ports
     */
    constructor(name, creator, workspace, image, container, ports) {
        this.name = name;
        this.creator = creator;
        this.workspace = workspace;
        this.image = image;
        this.container = container;
        this.ports = ports;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get workspace() {
        return this._workspace;
    }

    set workspace(value) {
        this._workspace = value;
    }

    get creator() {
        return this._creator;
    }

    set creator(value) {
        this._creator = value;
    }

    get ports() {
        return this._ports;
    }

    set ports(value) {
        this._ports = value;
    }

    get container() {
        return this._container;
    }

    set container(value) {
        this._container = value;
    }

    get image() {
        return this._image;
    }

    set image(value) {
        this._image = value;
    }

    /**
     * Convert object into a json
     *
     * @return {{creator: String, workspace: String, name: String}} JSON
     */
    toJSON() {
        return {
            name: this.name,
            creator: this.creator,
            workspace: this.workspace,
            image: this.image,
            container: this.container,
            ports: this.ports
        };
    }

    /**
     * Convert an object into a Project
     *
     * @param object Object
     * @return {Project} Project
     */
    static FromObject(object) {
        const project = new Project();

        if (object._key !== undefined) {
            project.key = object._key;
        }
        if (object.name !== undefined) {
            project.name = object.name;
        }
        if (object.creator !== undefined) {
            project.creator = object.creator;
        }
        if (object.workspace !== undefined) {
            project.workspace = object.workspace;
        }
        if (object.image !== undefined) {
            project.image = object.image;
        }
        if (object.container !== undefined) {
            project.container = object.container;
        }
        if (object.ports !== undefined) {
            project.ports = object.ports;
        }

        return project;
    }

    /**
     * Check a project, throw an error if project isn't valid
     *
     * @param project Project
     * @param mandatoryError Error when a mandatory field isn't defined
     */
    static Check(project, mandatoryError = true) {
        if (!(project instanceof Project)) {
            throw new ValidationError(PROJECT_API.INVALID_PROJECT);
        }

        if (project.name === undefined && mandatoryError) {
            throw new ValidationError(PROJECT_API.NAME_MANDATORY);
        } else if (project.name !== undefined && typeof project.name !== "string") {
            throw new ValidationError(PROJECT_API.INVALID_NAME);
        }

        if (project.creator === undefined && mandatoryError) {
            throw new ValidationError(PROJECT_API.CREATOR_MANDATORY);
        } else if (project.creator !== undefined && typeof project.creator !== "string") {
            throw new ValidationError(PROJECT_API.INVALID_CREATOR);
        } else if (project.creator !== undefined && typeof project.creator === "string" && !project.creator.startsWith(UserCollectionService.COLLECTION_NAME + "/")) {
            throw new ValidationError(PROJECT_API.INVALID_CREATOR_VALUE);
        }

        if (project.workspace === undefined && mandatoryError) {
            throw new ValidationError(PROJECT_API.WORKSPACE_MANDATORY);
        } else if (project.workspace !== undefined && typeof project.workspace !== "string") {
            throw new ValidationError(PROJECT_API.INVALID_WORKSPACE);
        } else if (project.workspace !== undefined && typeof project.workspace === "string" && !project.workspace.startsWith(WorkspaceCollectionService.COLLECTION_NAME + "/")) {
            throw new ValidationError(PROJECT_API.INVALID_WORKSPACE_VALUE);
        }

        if (project.image !== undefined && project.image !== null && typeof project.image !== "string") {
            throw new ValidationError(PROJECT_API.INVALID_IMAGE);
        }
        if (project.container !== undefined && project.container !== null && typeof project.container !== "string") {
            throw new ValidationError(PROJECT_API.INVALID_CONTAINER);
        }
        if (project.ports !== undefined && !(project.ports instanceof Array)) {
            throw new ValidationError(PROJECT_API.INVALID_PORTS);
        }
    }

    static get NAME_FIELD_NAME() {
        return "name";
    }

    static get WORKSPACE_FIELD_NAME() {
        return "workspace";
    }
}

export { Project };
