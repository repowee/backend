import { WORKSPACE_API } from "../common/errors";
import { UserCollectionService } from "../services/db/user";
import ValidationError from "./error";

/**
 * Workspace
 */
class Workspace {

    /**
     * Constructor
     *
     * @param name Name
     * @param owner Owner's id
     * @param network Docker network's id
     */
    constructor(name, owner, network) {
        this.name = name;
        this.owner = owner;
        this.network = network;
    }

    /**
     * Get key
     *
     * @return {string} Key
     */
    get key() {
        return this._key;
    }

    /**
     * Set key
     *
     * @param key Key
     */
    set key(key) {
        this._key = key;
    }

    /**
     * Get name
     *
     * @return {String} name
     */
    get name() {
        return this._name;
    }

    /**
     * Set name
     *
     * @param value Name
     */
    set name(value) {
        this._name = value;
    }

    /**
     * Get owner's id
     *
     * @return {String} Id
     */
    get owner() {
        return this._owner;
    }

    /**
     * Set owner
     *
     * @param value Owner's id
     */
    set owner(value) {
        this._owner = value;
    }

    /**
     * Get docker network's id
     *
     * @return {String} Id
     */
    get network() {
        return this._network;
    }

    /**
     * Set docker network
     *
     * @param value Network's id
     */
    set network(value) {
        this._network = value;
    }

    /**
     * Convert object into a json
     *
     * @return {{owner: String, name: String, network: String}} JSON
     */
    toJSON() {
        return {
            name: this.name,
            owner: this.owner,
            network: this.network
        };
    }

    /**
     * Convert an object into a Workspace
     *
     * @param object Object
     * @return {Workspace} Workspace
     */
    static FromObject(object) {
        const workspace = new Workspace();

        if (object instanceof Object) {
            if (object.name !== undefined) {
                workspace.name = object.name;
            }
            if (object.owner !== undefined) {
                workspace.owner = object.owner;
            }
            if (object.network !== undefined) {
                workspace.network = object.network;
            }
            if (object._key !== undefined) {
                workspace.key = object._key;
            }
        }

        return workspace;
    }

    /**
     * Check a workspace, throw an error if workspace isn't valid
     *
     * @param workspace Workspace
     * @param mandatoryError Error when a mandatory field isn't defined
     */
    static Check(workspace, mandatoryError = true) {
        if (!(workspace instanceof Workspace)) {
            throw new ValidationError(WORKSPACE_API.INVALID_WORKSPACE);
        }

        if (workspace.name === undefined && mandatoryError) {
            throw new ValidationError(WORKSPACE_API.NAME_MANDATORY);
        } else if (workspace.name !== undefined && typeof workspace.name !== "string") {
            throw new ValidationError(WORKSPACE_API.INVALID_NAME);
        }

        if (workspace.owner === undefined && mandatoryError) {
            throw new ValidationError(WORKSPACE_API.OWNER_MANDATORY);
        } else if (workspace.owner !== undefined && typeof workspace.owner !== "string") {
            throw new ValidationError(WORKSPACE_API.INVALID_OWNER);
        } else if (workspace.owner !== undefined && typeof workspace.owner === "string" && !workspace.owner.startsWith(UserCollectionService.COLLECTION_NAME + "/")) {
            throw new ValidationError(WORKSPACE_API.INVALID_OWNER_VALUE);
        }

        if (workspace.network === undefined && mandatoryError) {
            throw new ValidationError(WORKSPACE_API.NETWORK_MANDATORY);
        } else if (workspace.network !== undefined && typeof workspace.network !== "string") {
            throw new ValidationError(WORKSPACE_API.INVALID_NETWORK);
        }
    }

    static get NETWORK_PREFIX() {
        return "repowee_network/";
    }

    static get NAME_FIELD_NAME() {
        return "name";
    }

    static get OWNER_FIELD_NAME() {
        return "owner";
    }
}

export { Workspace };