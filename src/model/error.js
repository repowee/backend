import ServerError from "../common/servererror";

/**
 * Error when a model object isn't valid
 */
class ValidationError extends ServerError {

    /**
     * Constructor
     *
     * @param message Message
     */
    constructor(message) {
        super(message);
    }
}

export default ValidationError;