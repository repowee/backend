import { CronTime } from "cron";
import { docker as config } from "./config";

/**
 * Assert configuration
 *
 * @param config Configuration
 */
const assert = (config) => {
    // Check scheduler
    new CronTime(config.schedulerTick);

    // Check port range
    if (isNaN(config.portRangeStart)) {
        throw new Error("port_range_start isn't defined");
    }
    if (isNaN(config.portRangeEnd)) {
        throw new Error("port_range_end isn't defined");
    }
    if (config.portRangeEnd <= config.portRangeStart) {
        throw new Error(`Invalid range of ports: ${config.portRangeStart} -> ${config.portRangeEnd}`);
    }
};

if (!(config instanceof Object)) {
    throw new Error("Docker configuration isn't defined");
}

config.schedulerTick = config.scheduler_tick;
config.portRangeStart = parseInt(config.port_range_start);
config.portRangeEnd = parseInt(config.port_range_end);

// Assert config
assert(config);

export default config;