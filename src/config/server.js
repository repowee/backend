import ms from "ms";
import { server as config } from "./config";

/**
 * Function to assert configuration
 *
 * @param config Configuration
 */
const assert = (config) => {
    if (isNaN(config.port)) {
        throw new Error("port isn't defined");
    }

    if (typeof config.adminPassword !== "string") {
        throw new Error("admin_password isn't defined");
    }

    if (typeof config.cookieDuration !== "string") {
        throw new Error("cookie_duration isn't defined");
    } else {
        try {
            ms(config.cookieDuration);
        } catch (e) {
            throw new Error("Failed to interpret cookie_duration: '" + e.message + "'");
        }
    }

    if (typeof config.auth.tokenDuration !== "string") {
        throw new Error("token_duration isn't defined");
    } else {
        try {
            ms(config.auth.tokenDuration);
        } catch (e) {
            throw new Error("Failed to interpret token_duration: '" + e.message + "'");
        }
    }

    if (typeof config.auth.salt !== "string") {
        throw new Error("salt isn't defined");
    }
};

if (!(config instanceof Object)) {
    throw new Error("Server configuration isn't defined");
}

// Modify config
config.port = parseInt(config.port);
config.adminPassword = config.admin_password;
config.cookieDuration = config.cookie_duration;
config.auth = {
    tokenDuration: config.auth.token_duration,
    salt: config.auth.salt
};
config.tmpDir = typeof config.temporary_directory === "string" ? config.temporary_directory : "./tmp";
config.uploadLimits = config.upload_limits;

// Assert config
assert(config);

export default config;