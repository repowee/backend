import { application as config } from "./config";

/**
 * Function to assert configuration
 *
 * @param config Configuration
 */
const assert = (config) => {
    // Check workspace limit
    if (isNaN(config.workspacesLimit)) {
        throw new Error("workspaces_limit isn't defined");
    } else if (config.workspacesLimit < 0) {
        throw new Error("workspaces_limit is a negative number");
    }
};

if (!(config instanceof Object)) {
    throw new Error("Application configuration isn't defined");
}

config.workspacesLimit = parseInt(config.workspaces_limit);

assert(config);

export default config;