import { Database } from "arangojs";
import { LOGGER } from "../utils";
import { database as config } from "./config";

/**
 * Function to assert configuration
 *
 * @param config Configuration
 */
const assert = (config) => {
    if (typeof config.name !== "string") {
        LOGGER.warn("Main database isn't defined");
    }

    if (!(config.auth instanceof Object) || typeof config.auth.user !== "string") {
        throw new Error("Database user isn't defined");
    }
    if (!(config.auth instanceof Object) || typeof config.auth.password !== "string") {
        throw new Error("Database user's password isn't defined");
    }

    if (!(config.options instanceof Object)) {
        throw new Error("Database options isn't defined");
    }

    if (config.memory instanceof Object && config.memory.collections instanceof Array) {
        for (const collection of config.memory.collections) {
            if (typeof collection !== "string") {
                throw new Error("database.memory.collections must be an array of string");
            }
        }
    }
};

if (!(config instanceof Object)) {
    throw new Error("Database configuration isn't defined");
}

// Assert config
assert(config);

// Create database
const database = new Database(config.options);

// Define database
if (typeof config.name === "string") {
    database.useDatabase(config.name);
}

export { database, config };