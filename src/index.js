import cookieParser from "cookie-parser";
import cors from "cors";
import express from "express";
import upload from "express-fileupload";
import { Server } from "http";
import responseTime from "response-time";
import io from "socket.io";
import { HTTP } from "./common/errors";
import ServerError from "./common/servererror";
import { config as dbConfig, database } from "./config/database";
import serverConfig from "./config/server";
import { init as initRoutes, Response } from "./router/router";
import initServices from "./services";
import { start as startScheduler } from "./services/scheduler";
import { LOGGER, path } from "./utils";
import { environment, isProduction } from "./utils/node";
import { init as initSocket } from "./websocket/ws";

// Create application & websocket
const app = express();
const server = Server(app);
const socket = io(server, { path: "/ws" });

// Activate json pretty print (in dev mode)
if (!isProduction()) {
    app.set("json spaces", 4);
}

// Set up CORS
const corsConfig = { origin: true, credentials: true };
app.use(cors(corsConfig));
app.options("*", cors(corsConfig));

// Add cookie parser
app.use(cookieParser());

// Use JSON decoder for "application/json" body
app.use(express.json());

// Add fileupload middleware
app.use(upload({
    uriDecodeFileNames: true,
    safeFileNames: true,
    preserveExtension: true,
    abortOnLimit: true,
    limitHandler: (req, res, next) => next(new ServerError(HTTP.FILE_SIZE_REACHED)),
    useTempFiles: true,
    tempFileDir: serverConfig.tmpDir,
    debug: !isProduction(),
    limits: serverConfig.uploadLimits
}));

// Add response time
app.use(responseTime((req, res, time) => {
    LOGGER.info("[Express] " + req.method + " " + req.originalUrl + " in " + time.toFixed(3) + "ms => response code: " + res.statusCode);
}));

// Start app
server.listen(serverConfig.port, async () => {
    LOGGER.info("[Main] App listening on port : " + serverConfig.port + " (mode: " + environment() + ")");

    try {
        // Connect to database
        try {
            await database.login(dbConfig.auth.user, dbConfig.auth.password);
            LOGGER.info("[Main] Connect to database (url: " + dbConfig.options.url + ", db: " + dbConfig.name + ") successfully, with user: " + dbConfig.auth.user);
        } catch (e) {
            throw new Error("Failed to connect to database because: " + e.message);
        }

        // Init services here
        const services = await initServices();

        // Start scheduler
        startScheduler(services);

        // Clean-up tmp directory
        services.fs.cleanUpTmpDirectory();
        LOGGER.info(`[Main] Clean-up temporary directory ${serverConfig.tmpDir}`);

        // Set-up routes
        initRoutes(app, services, path(__dirname, "router", "routes"));

        // Set-up websocket
        initSocket(socket, services);

        // Set up error handler
        app.use((error, req, res, next) => {
            LOGGER.exception(error);

            // Send response
            res.status(error.code ? error.code : 500).json(new Response("KO", error.message));

            next();
        });

        server.emit("ready");
        LOGGER.info("[Main] Server is ready");
    } catch (e) {
        LOGGER.exception(e);
        process.exit(1);
    }
});

export default server;