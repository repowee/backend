import { WORKSPACE_API } from "../common/errors";
import ValidationError from "../model/error";
import { Workspace } from "../model/workspace";
import { UserCollectionService } from "./db/user";
import { UserService } from "./user";

/**
 * Workspace service
 */
class WorkspaceService {

    /**
     * Constructor
     *
     * @param {DBServiceContainer} dbContainer Database services
     * @param {DockerService} docker Docker service
     */
    constructor(dbContainer, docker) {
        this.dbService = dbContainer;
        this.docker = docker;
    }

    /**
     * Create a workspace
     *
     * @param owner Owner
     * @param object Workspace
     * @return {Promise<Workspace>} Saved workspace
     */
    async create(owner, object) {
        // Check limit
        const workspaces = await this.dbService.workspace.findByOwner(owner);
        if (owner.workspacesLimit !== 0 && workspaces.length + 1 > owner.workspacesLimit) {
            throw new ValidationError(WORKSPACE_API.LIMIT_REACHED);
        }

        // Convert into workspace & set owner
        const workspace = Workspace.FromObject(object);
        workspace.owner = UserCollectionService.Id(owner);
        workspace.key = undefined;

        // Create network
        const network = await this.docker.createNetwork();
        workspace.network = network.id;

        // Save workspace
        await this.dbService.workspace.save(workspace);
        return workspace;
    }

    /**
     * List workspaces of a given user
     *
     * @param user User
     * @return {Promise<Workspace[]>} Workspaces
     */
    async list(user) {
        // Find your workspaces
        const owned_workspaces = await this.dbService.workspace.findByOwner(user);

        // Set owner
        for (let i = 0; i < owned_workspaces.length; i++) {
            owned_workspaces[i].real_owner = user;
        }

        // Find invited ones
        const shared_workspaces = await this.dbService.workspace.findShared(user);

        return owned_workspaces.concat(shared_workspaces);
    }

    /**
     * Find a workspace with its owner and name
     *
     * @param owner Owner
     * @param name Workspace's name
     * @return {Promise<Workspace>} Workspace or undefined
     */
    async find(owner, name) {
        const workspaces = await this.dbService.workspace.findByOwnerAndName(owner, name);

        // Check existence
        if (workspaces.length === 0) {
            return undefined;
        }

        // Get network info
        workspaces[0].real_network = workspaces[0].network;
        workspaces[0].network = (await (await this.docker.network(workspaces[0].network)).inspect()).IPAM.Config[0];

        return workspaces[0];
    }

    /**
     * Update a given workspace
     *
     * @param workspace Workspace
     * @param new_workspace Workspace new data
     * @return {Promise<void>}
     */
    async update(workspace, new_workspace) {
        const update_workspace = new Workspace();

        // Update name
        if (new_workspace.name !== undefined) {
            update_workspace.name = new_workspace.name;
        }

        await this.dbService.workspace.update(workspace.key, update_workspace);
    }

    /**
     * Invite an user in a given workspace
     *
     * @param workspace Workspace
     * @param user User
     * @return {Promise<void>}
     */
    async invite(workspace, user) {
        await this.dbService.workspace.invite(workspace, user);
    }

    /**
     * Kick an user in a given workspace
     *
     * @param workspace Workspace
     * @param user User
     * @return {Promise<void>}
     */
    async kick(workspace, user) {
        await this.dbService.workspace.removeUser(workspace, user);
    }

    /**
     * Delete a workspace
     *
     * @param workspace Workspace
     * @return {Promise<void>}
     */
    async delete(workspace) {
        await this.dbService.workspace.delete(workspace);

        await this.dbService.project.deleteFromWorkspace(workspace);
    }

    /**
     * Transform a workspace into an API object
     *
     * @param workspace Workspace
     * @param owner Owner
     * @param users Users
     * @param projects Projects
     * @param network_config Network configuration
     * @return {Promise<Object>} API object
     */
    static ToAPIObject(workspace, owner, users, projects, network_config = {}) {
        const object = workspace.toJSON();

        // Delete network, replace owner and set users
        object.owner = UserService.ToAPIObject(owner);
        object.users = Object.values(users.map(UserService.ToAPIObject).reduce((map, user) => {
            map[user.mail] = user;
            return map;
        }, {}));
        object.network = network_config.Subnet;
        object.gateway = network_config.Gateway;
        object.projects = projects;

        return object;
    }
}

/**
 * Create workspace service
 *
 * @param {DBServiceContainer} dbContainer Database services
 * @param {DockerService} docker Docker service
 * @return {WorkspaceService} Service
 */
const create = (dbContainer, docker) => {
    return new WorkspaceService(dbContainer, docker);
};

export { create, WorkspaceService };