import { decode as decodeBase64 } from "base64-url";
import csvParser from "csv-parse/lib/sync";
import fs from "fs";
import { USER_API } from "../common/errors";
import ServerError from "../common/servererror";
import serverConfig from "../config/server";
import { User } from "../model/user";
import { URL_SELF_KEYWORD } from "../router/route";
import { hash, isHash } from "../utils/hash";
import { UserCollectionService } from "./db/user";

/**
 * User service
 */
class UserService {

    /**
     * Constructor
     *
     * @param {DBServiceContainer} dbContainer
     * @param {AuthenticationService} authService Auth service
     */
    constructor(dbContainer, authService) {
        this.dbService = dbContainer;
        this.authService = authService;
    }

    /**
     * Create an user
     *
     * @param object User
     * @return {User} Saved user
     */
    async create(object) {
        // Convert into an user
        const user = User.FromObject(object);

        // Transform user
        if (typeof user.name !== "string" && user.mail !== undefined) {
            user.name = user.mail.split("@")[0];
        }
        if (typeof user.password === "string" && !isHash(user.password)) {
            user.password = await hash(user.password, serverConfig.auth.salt);
        }
        user.auth = undefined;
        user.key = undefined;

        // Save user
        await this.dbService.user.save(user);
        return user;
    }

    /**
     * Create users from an uploaded CSV
     *
     * @param path CSV path
     * @return {Promise<User[]>} Users
     */
    async createFromCSV(path) {
        // Check path
        if (!fs.existsSync(path)) {
            throw new ServerError(USER_API.USERS_CSV_NOT_FOUND);
        }

        // Load CSV
        let users = csvParser(fs.readFileSync(path), {
            columns: true,
            comment: "#",
            delimiter: ";",
            skip_empty_lines: true,
            skip_lines_with_empty_values: true,
            trim: true
        });

        // Convert into users
        users = users.map(User.FromObject);

        // Check users
        users.forEach(User.Check);

        // Save users
        await this.dbService.user.service.executeTransaction([
            () => this.dbService.user.service.save(UserCollectionService.COLLECTION_NAME, ...users)
        ], { write: UserCollectionService.COLLECTION_NAME });

        return users;
    }

    /**
     * Get all users
     *
     * @return {Promise<User[]>} Users
     */
    all() {
        return this.dbService.user.all();
    }

    /**
     * Find an user with his id
     *
     * @param request HTTP request
     * @param id Mail or "me"
     * @return {User} User
     */
    async findById(request, id) {
        return await (id === URL_SELF_KEYWORD ? this.authService.findByToken(decodeBase64(request.cookies.token)) : this.dbService.user.findByMail(id));
    }

    /**
     * Update an user
     *
     * @param user Original user
     * @param new_user Partial user provided by the request
     * @param admin_rights Current user possess admin rights
     */
    async update(user, new_user, admin_rights) {
        // Update user
        if (new_user.name !== undefined) {
            user.name = new_user.name;
        }
        if (new_user.mail !== undefined) {
            user.mail = new_user.mail;
        }
        if (new_user.password !== undefined) {
            user.password = await hash(new_user.password, serverConfig.auth.salt);
        }
        if (new_user.workspacesLimit !== undefined && admin_rights) {
            user.workspacesLimit = new_user.workspacesLimit;
        }
        if (new_user.role !== undefined && admin_rights) {
            user.role = new_user.role;
        }

        // Save changes
        await this.dbService.user.update(user.key, user);
    }

    /**
     * Search users based on a given mail
     *
     * @param mail Mail
     * @return {Promise<User[]>} Users
     */
    async searchByMail(mail) {
        return await this.dbService.user.searchByMail(mail);
    }

    /**
     * Remove an user
     *
     * @param user User
     */
    async remove(user) {
        // Check workspaces
        if ((await this.dbService.workspace.findByOwner(user)).length !== 0) {
            throw new ServerError(USER_API.WORKSPACE_OWNER);
        }

        // Remove
        await this.dbService.user.remove(user);
    }

    /**
     * Transform an user into an API object
     *
     * @param user User
     * @return {Object} API object
     */
    static ToAPIObject(user) {
        const object = user.toJSON();

        // Remove sensitive fields
        delete object.password;
        delete object.auth;

        return object;
    }
}

/**
 * Create user service
 *
 * @param {DBServiceContainer} dbContainer Database container
 * @param {AuthenticationService} authService Auth service
 * @return {UserService} Service
 */
const create = (dbContainer, authService) => {
    return new UserService(dbContainer, authService);
};

export { create, UserService };