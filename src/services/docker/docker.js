import dockerode from "dockerode";
import fs from "fs";
import glob from "glob";
import yaml from "js-yaml";
import moment from "moment";
import path from "path";
import stream from "stream";
import { v4 as uuidv4 } from "uuid";
import { REPOWEE_CONFIG } from "../../common/errors";
import ServerError from "../../common/servererror";
import docker from "../../config/docker";
import { Workspace } from "../../model/workspace";
import { LOGGER } from "../../utils";
import { DateUtilities } from "../../utils/database";
import { environment } from "../../utils/node";

const dockerImageConfigFileName = "repowee.yaml";
const dockerImageAndContainerPrefix = "repowee_";

/**
 * Basic service for docker manipulation
 */
class DockerService {

    /**
     * Constructor
     *
     * @param options Options
     */
    constructor(options) {
        this.docker = new dockerode(options);
    }

    /**
     * Create a docker image and build if from a project folder path and his config file
     *
     * @param {String} projectPath Path of a project
     *
     * A config file named repowee.yaml is requiered in the project folder to build a docker image and to run it
     * An exemple is available in the /dev/dockerExemple folder
     *
     * The atributes of this config file are :
     *
     * image :
     *    base            [String]    Image name from where the layer will be created
     *    entrypoint      [String]    Docker entryPoint
     *    ports           [Integer list]    (Optional) List of ports to be expose by docker
     *    commands        [String list]     (Optional) Tasks to be runed by docker
     *
     * project:
     *    workingDirectory   [String]    Name of the image work directory
     *    files              [String list]     (Optional) files that will be added in the workDir
     *
     * @returns {String} Name of the built image and ports
     */
    async buildImageFromFolder(projectPath, socket) {

        let config = DockerService.checkConfigFile(projectPath);

        this.createDockerfile(projectPath, config);

        const image = await this.buildDockerImage(projectPath, config, socket);

        return {
            image: image,
            ports: config.image.ports
        };
    }


    /**
     * Check the validity of the given docker parameters
     * @param {String} projectPath Path of a project
     * @return {object} Config file
     */
    static checkConfigFile(projectPath) {
        // Check folder
        if (!fs.existsSync(projectPath)) throw new Error(REPOWEE_CONFIG.CONFIG.PROJECT_FOLDER_NOT_FOUND);

        // Check Config file
        if (!fs.existsSync(projectPath + dockerImageConfigFileName))
            throw new Error(REPOWEE_CONFIG.CONFIG.PROJECT_CONFIG_NOT_FOUND);

        // import config
        let config = yaml.safeLoad(fs.readFileSync(projectPath + dockerImageConfigFileName, "utf8"));
        if (config == undefined) throw new Error("Cant read config file. Is the config file emtpy ?");

        // required Parameters
        if (typeof config.image.base !== "string") {
            throw new Error(REPOWEE_CONFIG.CONFIG.BASE_IMAGE_UNDEFINED);
        }
        if (!(config.project instanceof Object)) {
            throw new Error(REPOWEE_CONFIG.CONFIG.PROJECT_SECTION_UNDEFINED);
        }
        if (typeof config.project.workingDirectory !== "string") {
            throw new Error(REPOWEE_CONFIG.CONFIG.WORK_DIR_UNDEFINED);
        }


        // Check project files
        if ("files" in config.project) {
            config.project.files.forEach(file => {
                // check if file is in gis project scope
                let absPath = path.resolve(projectPath, file);
                let projectAbsPath = path.resolve(projectPath);
                if (!absPath.startsWith(projectAbsPath)) {
                    throw new Error(REPOWEE_CONFIG.CONFIG.INVALID_FILE_PATH);
                }
            });
        } else {
            config.project.files = [];
        }

        // Ports
        if ("ports" in config.image) {
            // Range
            config.image.ports.forEach(port => {
                if (isNaN(port) || !Number.isInteger(parseInt(port))) throw new Error(REPOWEE_CONFIG.CONFIG.INVALID_PORT);
                if (port < 0 || port > 65535) throw new Error(REPOWEE_CONFIG.CONFIG.INVALID_PORT_NUMBER);
            });
        }

        return config;
    }

    /**
     * create the project docker file
     * @param {String} projectPath    Path of a project
     * @param {object} config         project config file
     */
    createDockerfile(projectPath, config) {

        // Copy command creation
        let copyCommand = "";
        if ("files" in config.project && config.project.files.length) {
            config.project.files.forEach(file => {
                copyCommand += "COPY " + file + " ./\n";
            });
        }

        // Run command creation
        let runCommands = "";
        if ("commands" in config.image && config.image.commands.length) {
            config.image.commands.forEach(run => {
                runCommands += "RUN " + run + "\n";
            });
        }

        // Port Expose command creation
        let portsCommand = "";
        if ("ports" in config.image && config.image.ports.length) {
            portsCommand = "EXPOSE " + config.image.ports.join(" ");
        }


        let dockerFileContent = `
FROM ${config.image.base}

WORKDIR ${config.project.workingDirectory}

${copyCommand}

${runCommands}

${portsCommand}

${config.image.entrypoint ? "CMD " + config.image.entrypoint : ""}
    `;

        // Create / replace the docker file
        fs.writeFileSync(projectPath + "Dockerfile", dockerFileContent);


    }

    /**
     * Build an image from the project
     * @param {String} projectPath    Path of a project
     * @param {object} config         project config file
     * @returns {string} Docker Image Name
     */
    async buildDockerImage(projectPath, config, socket) {
        // Generating the docker image Name
        let imageName = (dockerImageAndContainerPrefix + uuidv4()) + ":latest";

        let stream = await this.docker.buildImage({ context: projectPath, src: [ ...config.project.files.map(f => glob.sync(f, { cwd: projectPath })).flatMap(f => f), "Dockerfile" ] }, { t: imageName });

        let build_report = undefined;
        await new Promise((resolve, reject) => {
            this.docker.modem.followProgress(stream, (error, result) => {
                build_report = result;

                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            }, event => {
                if (event.stream) {
                    LOGGER.info(`[Docker-Build] [${imageName}] ${event.stream}`);
                    if (socket) {
                        socket.emit("console:data", `[INFO] [${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}] ${event.stream}`);
                    }
                } else if (event.error) {
                    LOGGER.error(`[Docker-Build] [${imageName}] ${event.error}`);
                    if (socket) {
                        socket.emit("console:data", `[ERROR] [${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}] ${event.error}`);
                    }
                }
            });
        });

        if (build_report.filter(e => e.error).length !== 0) {
            throw new ServerError(REPOWEE_CONFIG.FAILED_TO_BUILD);
        }

        return imageName;

    }

    /**
     * Start a container from the name of a built image
     *
     * @param {String} imageName name of the image to run
     * @returns {Promise<String>}
     */
    runContainer(imageName, ports, socket) {
        let containerName = (dockerImageAndContainerPrefix + uuidv4());

        const host_config = { PortBindings: {} };
        for (let i = 0; i < ports.length; i++) {
            host_config.PortBindings[ports[i].from + "/tcp"] = [ { HostPort: ports[i].to + "" } ];
        }

        // Log into socket
        const logStream = new stream.PassThrough();
        logStream.on("data", (chunk) => {
            LOGGER.info(`[Docker-Run] [${imageName}] ${chunk.toString("utf8")}`);
            if (socket) {
                socket.emit("console:data", `[INFO] [${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}] ${chunk.toString("utf8")}`);
            }
        });

        return this.docker.createContainer({
            Image: imageName,
            name: containerName,
            HostConfig: host_config
        }).then((container) => {
            return container.start().then(() => {
                container.logs({
                    follow: true,
                    stdout: true,
                    stderr: true
                }, function (err, stream) {
                    if (err) {
                        return LOGGER.error(err.message);
                    }
                    container.modem.demuxStream(stream, logStream, logStream);
                    stream.on("end", () => {
                        LOGGER.info("[Docker] Container " + container.id + " stopped");
                        if (socket) {
                            socket.emit("console:data", "Stopped!");
                        }
                    });
                });

                return container.id;
            });
        });
    }

    /**
     * Return the list of the repowee images
     * description of an image object here https://docs.docker.com/engine/api/v1.37/#operation/ImageList
     *
     * The 'RepoDigests' attribut is the image name
     * @returns {Promise<Array>}
     */
    getAllImages() {
        const opts = { filters: { reference: {} } };
        opts.filters.reference[dockerImageAndContainerPrefix + "*"] = true;

        return this.docker.listImages(opts);
    }

    /**
     * Return the list of running containers
     * more info here : https://docs.docker.com/engine/api/v1.37/#operation/ContainerList
     * @returns {Promise<Array>}
     */
    getAllContainers() {
        const opts = { filters: { name: {} } };
        opts.filters.name[dockerImageAndContainerPrefix] = true;

        return this.docker.listContainers(opts);
    }

    /**
     * Remove a docker image
     * @param {String} imageName ID of the container to delete
     * @returns {Promise}
     */
    removeImage(imageId) {
        return this.docker.getImage(imageId).remove({ force: true });
    }

    /**
     * Remove all Repowee docker images
     * @returns {Promise<Array><Object>} Objects description : https://docs.docker.com/engine/api/v1.37/#operation/ImageTag
     */
    removeAllImages() {
        return this.getAllImages().then(images => {
            let promises = images.map((image) => this.removeImage(image.Id));
            return Promise.all(promises);
        });
    }

    /**
     * Stop a container
     * @param {number} containerId ID of the container
     * @returns {Promise}
     */
    stopContainer(containerId) {
        return this.docker.getContainer(containerId).stop();
    }

    /**
     * Stop a container
     * @param {number} containerId ID of the container
     * @returns {Promise}
     */
    startContainer(containerId, socket) {
        // Log into socket
        const logStream = new stream.PassThrough();
        logStream.on("data", (chunk) => {
            LOGGER.info(`[Docker-Run] [${containerId}] ${chunk.toString("utf8")}`);
            if (socket) {
                socket.emit("console:data", `[INFO] [${moment().format("YYYY-MM-DD HH:mm:ss.SSS")}] ${chunk.toString("utf8")}`);
            }
        });

        const container = this.docker.getContainer(containerId);
        return container.start().then(() => {
            container.logs({
                follow: true,
                stdout: true,
                stderr: true
            }, function (err, stream) {
                if (err) {
                    return LOGGER.error(err.message);
                }
                container.modem.demuxStream(stream, logStream, logStream);
                stream.on("end", () => {
                    LOGGER.info("[Docker] Container " + container.id + " stopped");
                    if (socket) {
                        socket.emit("console:data", "Stopped!");
                    }
                });
            });
        });
    }

    /**
     * Stop all created containers
     * @param {number} containerId ID of the container to delete
     * @returns {Promise}
     */
    stopAllContainers() {
        return this.getAllContainers().then(containers => {
            let promises = containers.map((container) => this.docker.getContainer(container.Id).stop());

            return Promise.all(promises);
        });
    }

    /**
     * Create a network
     *
     * @return {Promise<Network>} Network
     */
    async createNetwork() {
        return this.docker.createNetwork({
            name: Workspace.NETWORK_PREFIX + uuidv4(),
            labels: {
                created: moment().format(DateUtilities.FORMAT)
            }
        });
    }

    /**
     * Get all networks
     *
     * @return {Promise<Network[]>} Networks
     */
    async networks() {
        const opts = { filters: { name: {} } };
        opts.filters.name[Workspace.NETWORK_PREFIX] = true;

        return this.docker.listNetworks(opts);
    }

    /**
     * Get a network with its id
     *
     * @param id Network's id
     * @return {Promise<Network>} Network
     */
    async network(id) {
        return this.docker.getNetwork(id);
    }
}

/**
 * Create docker service
 *
 * @return {DockerService} Service
 */
const create = async () => {
    const service = new DockerService(docker.options);

    // Test docker connection
    if (environment() !== "ci") {
        await service.docker.info();
        LOGGER.info("[Services] Connect to docker");
    }

    return service;
};

export { create, DockerService };