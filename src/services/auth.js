import { decode as decodeBase64 } from "base64-url";
import { sign, TokenExpiredError, verify } from "jsonwebtoken";
import AuthError from "../common/autherror";
import { AUTH } from "../common/errors";
import ServerError from "../common/servererror";
import serverConfig from "../config/server";
import { AuthInfo } from "../model/user";
import { LOGGER } from "../utils";
import { hash } from "../utils/hash";

const LOG_PREFIX = "[Auth-Service] ";

/**
 * Authentication service
 */
class AuthenticationService {

    /**
     * Constructor
     *
     * @param {DBServiceContainer} dbContainer
     */
    constructor(dbContainer) {
        this.userService = dbContainer.user;
    }

    /**
     * Login an user
     *
     * @param login User's email
     * @param password User's password
     * @return string User's token
     */
    async login(login, password) {
        // Check arguments
        if (login === undefined) {
            throw new ServerError(AUTH.EMAIL_UNDEFINED);
        }
        if (password === undefined) {
            throw new ServerError(AUTH.PASSWORD_UNDEFINED);
        }

        LOGGER.info(LOG_PREFIX + "Attempt to login: " + login);

        // Find user
        const passwordHash = await hash(password, serverConfig.auth.salt);
        const users = await this.userService.findByAuthInfo(login, passwordHash);

        // Check results
        if (users.length === 0) {
            throw new AuthError(AUTH.INVALID_CREDENTIALS);
        }
        if (users.length > 1) {
            throw new ServerError(AUTH.MULTIPLE_USER_MATCHING);
        }
        const user = users[0];

        // Generate a token
        const token = sign({
            data: await hash(user.mail + user.password, serverConfig.auth.salt)
        }, serverConfig.auth.salt, { expiresIn: serverConfig.auth.tokenDuration });

        // Update token
        if (user.auth instanceof AuthInfo) {
            user.auth.token = token;
        } else {
            user.auth = new AuthInfo(token);
        }
        LOGGER.info(LOG_PREFIX + "Create a token for " + login);

        // Save auth info
        await this.userService.update(user.key, user);

        return token;
    }

    /**
     * Find an user with his token
     *
     * @param token Token
     * @return {Promise<User>} User
     */
    async findByToken(token) {
        return (await this.userService.findBy("auth.token", token))[0];
    }

    /**
     * Verify auth token, throws an error if token is invalid
     *
     * @param token Token
     * @param assertUser Predicate to assert logged user
     */
    async verifyToken(token, assertUser = () => true) {
        // Decode token
        const t = decodeBase64(token);

        // Check token & if token exists in database
        try {
            verify(t, serverConfig.auth.salt);

            // Find user
            const user = await this.findByToken(t);

            // Assert user
            if (user === undefined) {
                throw new AuthError(AUTH.UNKNOWN_TOKEN);
            }
            if (!(await assertUser(user))) {
                throw new AuthError(AUTH.UNAUTHORIZED_USER);
            }
        } catch (e) {
            if (e instanceof TokenExpiredError) {
                throw new AuthError(AUTH.EXPIRED_TOKEN);
            } else {
                throw new ServerError(e.message, e.code ? e.code : ServerError.DefaultErrorCode);
            }
        }
    }
}

/**
 * Create authentication service
 *
 * @param {DBServiceContainer} dbContainer Database container
 * @return {AuthenticationService} Service
 */
const create = (dbContainer) => {
    return new AuthenticationService(dbContainer);
};

export { create, AuthenticationService };