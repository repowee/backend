import { create as createAuthServer } from "./auth";
import { create as createProjectDBService } from "./db/project";
import { create as createUserDBService } from "./db/user";
import { create as createWorkspaceDBService } from "./db/workspace";
import { create as createDockerService } from "./docker/docker";
import { create as createFSService } from "./fs";
import { create as createProjectService } from "./project";
import { create as createUserService } from "./user";
import { create as createWorkspaceService } from "./workspace";

/**
 * Service container
 */
class ServiceContainer {

    /**
     * Get DB services container
     *
     * @return {DBServiceContainer} container
     */
    get db() {
        return this._db;
    }

    /**
     * Set DB services container
     *
     * @param db Service container
     */
    set db(db) {
        this._db = db;
    }

    /**
     * Get authentication service
     *
     * @return {AuthenticationService} Service
     */
    get auth() {
        return this._auth;
    }

    /**
     * Set authentication service
     *
     * @param service Service
     */
    set auth(service) {
        this._auth = service;
    }

    /**
     * Get user service
     *
     * @return {UserService} Service
     */
    get user() {
        return this._user;
    }

    /**
     * Set user service
     *
     * @param service Service
     */
    set user(service) {
        this._user = service;
    }

    /**
     * Get docker service
     *
     * @return {DockerService} Service
     */
    get docker() {
        return this._docker;
    }

    /**
     * Set docker service
     *
     * @param service Service
     */
    set docker(service) {
        this._docker = service;
    }

    /**
     * Get workspace service
     *
     * @return {WorkspaceService} Service
     */
    get workspace() {
        return this._workspace;
    }

    /**
     * Set workspace service
     *
     * @param service Service
     */
    set workspace(service) {
        this._workspace = service;
    }

    /**
     * Get fs service
     *
     * @return {FileSystemService} Service
     */
    get fs() {
        return this._fs;
    }

    /**
     * Set fs service
     *
     * @param service Service
     */
    set fs(service) {
        this._fs = service;
    }

    /**
     * Get project service
     *
     * @return {ProjectService} Service
     */
    get project() {
        return this._project;
    }

    /**
     * Set project service
     *
     * @param service Service
     */
    set project(service) {
        this._project = service;
    }
}

/**
 * DB service container
 */
class DBServiceContainer {

    /**
     * Constructor
     *
     * @param user User service
     * @param workspace Workspace service
     * @param project Project service
     */
    constructor(user, workspace, project) {
        this.user = user;
        this.workspace = workspace;
        this.project = project;
    }

    /**
     * Get user service
     *
     * @return {UserCollectionService} Service
     */
    get user() {
        return this._user;
    }

    /**
     * Set user service
     *
     * @param value Service
     */
    set user(value) {
        this._user = value;
    }

    /**
     * Get workspace service
     *
     * @return {WorkspaceCollectionService} Service
     */
    get workspace() {
        return this._workspace;
    }

    /**
     * Set workspace service
     *
     * @param value Service
     */
    set workspace(value) {
        this._workspace = value;
    }

    /**
     * Get workspace service
     *
     * @return {ProjectCollectionService} Service
     */
    get project() {
        return this._project;
    }

    /**
     * Set project service
     *
     * @param value Service
     */
    set project(value) {
        this._project = value;
    }
}

/**
 * Init service container
 *
 * @return {Promise<ServiceContainer>} Container
 */
const init = async () => {
    const container = new ServiceContainer();

    // DB
    container.db = new DBServiceContainer(await createUserDBService(), await createWorkspaceDBService(), await createProjectDBService());

    // Others
    container.docker = await createDockerService();
    container.auth = createAuthServer(container.db);
    container.user = createUserService(container.db, container.auth);
    container.workspace = createWorkspaceService(container.db, container.docker);
    container.project = createProjectService(container.db, container.docker);
    container.fs = createFSService();

    return container;
};

export default init;