import { aql } from "arangojs";
import serverConfig from "../../config/server";
import { User, UserRole } from "../../model/user";
import { LOGGER } from "../../utils";
import { create as createService } from "./service";

/**
 * Function to create admin if he doesn't exist
 *
 * @param service Service
 */
const createAdmin = async (service) => {
    const user = await service.findByMail(UserCollectionService.ADMIN_MAIL);

    // Create admin
    if (user === undefined) {
        await service.save(new User("Administrator", UserCollectionService.ADMIN_MAIL, serverConfig.adminPassword, UserRole.ADMIN));

        LOGGER.info("[User-Collection] Create administrator");
    }
};

/**
 * Function to create mail index if it doesn't exist
 *
 * @param service Service
 */
const createIndex = async (service) => {
    const index = await service.service.indexBy(UserCollectionService.COLLECTION_NAME, index => index.name === UserCollectionService.MAIL_INDEX_NAME);

    // Create index
    if (index === undefined) {
        await service.service.createIndex(UserCollectionService.COLLECTION_NAME, {
            name: UserCollectionService.MAIL_INDEX_NAME,
            fields: [ User.MAIL_FIELD_NAME ],
            unique: true,
            type: "hash"
        });

        LOGGER.info("[User-Collection] Create mail index");
    }
};

/**
 * User collection service
 */
class UserCollectionService {

    /**
     * Constructor
     *
     * @param service Database service
     */
    constructor(service) {
        this.service = service;
    }

    /**
     * Get service
     *
     * @return {BasicDBService} Database service
     */
    get service() {
        return this._service;
    }

    /**
     * Set service
     *
     * @param service Database service
     */
    set service(service) {
        this._service = service;
    }

    /**
     * Find an user by mail
     *
     * @param mail Mail
     * @return {Promise<User>} User or undefined
     */
    async findByMail(mail) {
        const users = await this.service.findBy(UserCollectionService.COLLECTION_NAME, User.MAIL_FIELD_NAME, mail);

        return users.length === 0 ? undefined : UserCollectionService.ToUser(users[0]);
    }

    /**
     * Find an user with his key
     *
     * @param key Key
     * @return {Promise<User>} User
     */
    async findByKey(key) {
        return UserCollectionService.ToUser(await this.service.findByKey(UserCollectionService.COLLECTION_NAME, key));
    }

    /**
     * Find an user with his authentication info
     *
     * @param login User's login
     * @param password User's password
     * @return {Promise<User[]>} Users
     */
    async findByAuthInfo(login, password) {
        return (await this.service.query(aql`
                FOR user in ${this.service.collection(UserCollectionService.COLLECTION_NAME)}
                    FILTER user.mail != null && user.password != null && user.mail == ${login} && user.password == ${password}
                    RETURN user
            `)
        ).map(UserCollectionService.ToUser);
    }

    /**
     * Get all users
     *
     * @return {Promise<Array<User>>} Users
     */
    async all() {
        return (await this.service.all(UserCollectionService.COLLECTION_NAME)).map(UserCollectionService.ToUser);
    }

    /**
     * Save users and set user.key
     *
     * @param users Users
     */
    async save(...users) {
        const objects = [ ...users ];
        objects.forEach(User.Check);

        // Save
        const info = await this.service.save(UserCollectionService.COLLECTION_NAME, ...objects);

        // Set key
        for (let i = 0; i < info.length; i++) {
            objects[i].key = info[i]._key;

            LOGGER.debug(`[User-Collection] Save ${objects[i].key}/${objects[i].mail}`);
        }
    }

    /**
     * Update an user
     *
     * @param key User's key
     * @param user User or partial user
     */
    async update(key, user) {
        // Check user
        User.Check(user, false);

        await this.service.executeTransaction([
            () => this.service.update(UserCollectionService.COLLECTION_NAME, key, user),
            () => LOGGER.info(`[User-Collection] Update ${key}/${user.mail}`)
        ], { write: UserCollectionService.COLLECTION_NAME });
    }

    /**
     * Remove an user
     *
     * @param user User
     */
    async remove(user) {
        await this.service.executeTransaction([
            () => this.service.remove(UserCollectionService.COLLECTION_NAME, user.key),
            () => LOGGER.info(`[User-Collection] Remove ${user.key}/${user.mail}`)
        ], { write: UserCollectionService.COLLECTION_NAME });
    }

    /**
     * Find users with field matching or not value
     *
     * @param field Field
     * @param value Value
     * @param equals Field's value must match value
     * @return {Promise<User[]>} Users
     */
    async findBy(field, value, equals) {
        return (await this.service.findBy(UserCollectionService.COLLECTION_NAME, field, value, equals)).map(UserCollectionService.ToUser);
    }

    /**
     * Search users based on a given mail
     *
     * @param mail Mail
     * @return {Promise<User[]>} Users
     */
    async searchByMail(mail) {
        return (await this.service.query(aql`
                FOR user in ${this.service.collection(UserCollectionService.COLLECTION_NAME)}
                    FILTER user.mail LIKE ${"%" + mail + "%"}
                    RETURN user
            `)
        ).map(UserCollectionService.ToUser);
    }

    /**
     * Convert an object into an User with a check
     *
     * @param object Object
     * @return {User} User
     */
    static ToUser(object) {
        const user = User.FromObject(object);

        User.Check(user);
        return user;
    }

    /**
     * Get user's id
     *
     * @param user User
     */
    static Id(user) {
        return UserCollectionService.COLLECTION_NAME + "/" + user.key;
    }

    static get COLLECTION_NAME() {
        return "users";
    }

    static get ADMIN_MAIL() {
        return "admin@repowee.fr";
    }

    static get MAIL_INDEX_NAME() {
        return "mail-index";
    }
}

/**
 * Create service
 *
 * @return {Promise<UserCollectionService>} User service
 */
const create = async () => {
    const service = new UserCollectionService(await createService(UserCollectionService.COLLECTION_NAME));

    // Create index
    await createIndex(service);

    // Create admin
    await createAdmin(service);

    return service;
};

export { create, UserCollectionService };