import { aql } from "arangojs";
import { database } from "../../config/database";
import { Project } from "../../model/project";
import { User } from "../../model/user";
import { Workspace } from "../../model/workspace";
import { LOGGER } from "../../utils";
import { edgeCollection } from "../../utils/database";
import { ProjectCollectionService } from "./project";
import { create as createService } from "./service";
import { UserCollectionService } from "./user";

class WorkspaceCollectionService {

    /**
     * Constructor
     *
     * @param service Database service
     */
    constructor(service) {
        this.service = service;
    }

    /**
     * Get service
     *
     * @return {BasicDBService} Database service
     */
    get service() {
        return this._service;
    }

    /**
     * Set service
     *
     * @param service Database service
     */
    set service(service) {
        this._service = service;
    }

    /**
     * Find user's workspaces
     *
     * @param user User
     * @return {Promise<Workspace[]>} Workspaces
     */
    async findByOwner(user) {
        let workspaces = await (await database.query(aql`
            FOR workspace in ${this.service.collection(WorkspaceCollectionService.COLLECTION_NAME)}
                FILTER workspace.owner == ${UserCollectionService.Id(user)}
                
                RETURN {
                    _key: workspace._key,
                    name: workspace.name,
                    owner: workspace.owner,
                    network: workspace.network,
                    users: (
                        FOR user IN 1..1 INBOUND workspace ${this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME)}
                            RETURN user
                    ),
                    projects: (
                        FOR project IN 1..1 OUTBOUND workspace ${this.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME)}
                            RETURN {
                                name: project.name
                            }
                    )
                }`)
        ).all();

        for (let i = 0; i < workspaces.length; i++) {
            const users = workspaces[i].users;
            const projects = workspaces[i].projects;

            workspaces[i] = Workspace.FromObject(workspaces[i]);
            workspaces[i].users = users.map(User.FromObject);
            workspaces[i].projects = projects.map(Project.FromObject);
        }

        return workspaces;
    }

    /**
     * Find workspaces where user was invited
     *
     * @param user User
     * @return {Promise<Workspace[]>} Workspaces
     */
    async findShared(user) {
        let workspaces = await (await database.query(aql`
            FOR user in ${this.service.collection(UserCollectionService.COLLECTION_NAME)}
                FILTER user.mail == ${user.mail}
                
                FOR workspace IN 1..1 OUTBOUND user ${this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME)}
                    RETURN {
                        _key: workspace._key,
                        name: workspace.name,
                        owner: workspace.owner,
                        real_owner: DOCUMENT(workspace.owner),
                        network: workspace.network,
                        users: (
                            FOR invited_user IN 1..1 INBOUND workspace ${this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME)}
                                RETURN invited_user
                        ),
                        projects: (
                            FOR project IN 1..1 OUTBOUND workspace ${this.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME)}
                                RETURN {
                                    name: project.name
                                }
                        )
                    }
                `)
        ).all();

        for (let i = 0; i < workspaces.length; i++) {
            const users = workspaces[i].users;
            const owner = User.FromObject(workspaces[i].real_owner);
            const projects = workspaces[i].projects;

            workspaces[i] = Workspace.FromObject(workspaces[i]);
            workspaces[i].real_owner = owner;
            workspaces[i].users = users.map(User.FromObject);
            workspaces[i].projects = projects.map(Project.FromObject);
        }

        return workspaces;
    }

    async findByOwnerAndName(owner, name) {
        let workspaces = await (await database.query(aql`
            FOR workspace in ${this.service.collection(WorkspaceCollectionService.COLLECTION_NAME)}
                FILTER workspace.owner == ${UserCollectionService.Id(owner)} AND workspace.name == ${name}
                
                RETURN {
                    _key: workspace._key,
                    name: workspace.name,
                    owner: workspace.owner,
                    real_owner: DOCUMENT(workspace.owner),
                    network: workspace.network,
                    users: (
                        FOR invited_user IN 1..1 INBOUND workspace ${this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME)}
                        RETURN invited_user
                    ),
                    projects: (
                        FOR project IN 1..1 OUTBOUND workspace ${this.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME)}
                            RETURN {
                                name: project.name
                            }
                    )
                }
                `)
        ).all();

        for (let i = 0; i < workspaces.length; i++) {
            const users = workspaces[i].users;
            const projects = workspaces[i].projects;

            workspaces[i] = Workspace.FromObject(workspaces[i]);
            workspaces[i].real_owner = User.FromObject(workspaces[i].real_owner);
            workspaces[i].users = users.map(User.FromObject);
            workspaces[i].projects = projects.map(Project.FromObject);
        }

        return workspaces;
    }

    /**
     * Save workspaces
     *
     * @param workspaces Workspaces
     */
    async save(...workspaces) {
        const objects = [ ...workspaces ];
        objects.forEach(Workspace.Check);

        // Save
        const info = await this.service.save(WorkspaceCollectionService.COLLECTION_NAME, ...workspaces);

        // Set key
        for (let i = 0; i < info.length; i++) {
            objects[i].key = info[i]._key;

            LOGGER.debug(`[Workspace-Collection] Save ${objects[i].key}/${objects[i].owner.split("/")[1]}/${objects[i].name}`);
        }
    }

    /**
     * Update a workspace
     *
     * @param key Workspace's key
     * @param workspace Workspace
     */
    async update(key, workspace) {
        Workspace.Check(workspace, false);

        await this.service.executeTransaction([
            () => this.service.update(WorkspaceCollectionService.COLLECTION_NAME, key, workspace)
        ], { write: WorkspaceCollectionService.COLLECTION_NAME });
    }

    /**
     * Invite an user in a given workspace
     *
     * @param workspace Workspace
     * @param user User
     * @return {Promise<void>}
     */
    async invite(workspace, user) {
        await this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME).save({
            _from: UserCollectionService.Id(user),
            _to: WorkspaceCollectionService.Id(workspace)
        });
    }

    /**
     * Remove the link between a workspace and an user
     *
     * @param workspace Workspace
     * @param user User
     * @return {Promise<void>}
     */
    async removeUser(workspace, user) {
        // Get links
        let links = await (await database.query(aql`
            FOR link in ${this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME)}
                FILTER link._from == ${UserCollectionService.Id(user)}
                
                RETURN link`)
        ).all();

        // Remove links
        for (let link of links) {
            await this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME).remove(link);
        }
    }

    /**
     * Delete a workspace
     *
     * @param workspace Workspace
     * @return {Promise<void>}
     */
    async delete(workspace) {
        // Get links
        let links = await (await database.query(aql`
            FOR link in ${this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME)}
                FILTER link._to == ${WorkspaceCollectionService.Id(workspace)}
                
                RETURN link._key`)
        ).all();

        // Delete
        await this.service.collection(WorkspaceCollectionService.LINK_COLLECTION_NAME).removeByKeys(links);
        await this.service.collection(WorkspaceCollectionService.COLLECTION_NAME).remove(workspace.key);
    }

    /**
     * Remove a workspace
     *
     * @param workspace Workspace
     */
    async remove(workspace) {
        await this.service.executeTransaction([
            () => this.service.remove(WorkspaceCollectionService.COLLECTION_NAME, workspace.key)
        ], { write: WorkspaceCollectionService.COLLECTION_NAME });
    }

    /**
     * Get all workspaces
     *
     * @return {Promise<Workspace[]>} Workspaces
     */
    all() {
        return this.service.all(WorkspaceCollectionService.COLLECTION_NAME);
    }

    /**
     * Get workspace's id
     *
     * @param workspace Workspace
     */
    static Id(workspace) {
        return WorkspaceCollectionService.COLLECTION_NAME + "/" + workspace.key;
    }

    /**
     * Convert an object into a workspace with a check
     *
     * @param object Object
     * @return {Workspace} Workspace
     */
    static ToWorkspace(object) {
        const workspace = Workspace.FromObject(object);

        Workspace.Check(workspace);
        return workspace;
    }

    static get COLLECTION_NAME() {
        return "workspaces";
    }

    static get LINK_COLLECTION_NAME() {
        return "users_to_workspaces";
    }

    static get INDEX_NAME() {
        return "workspace-index";
    }
}

/**
 * Create index
 *
 * @param service
 */
const createIndex = async (service) => {
    const index_col = await service.service.indexBy(WorkspaceCollectionService.COLLECTION_NAME, index => index.name === WorkspaceCollectionService.INDEX_NAME);

    // Create indices
    if (index_col === undefined) {
        await service.service.createIndex(WorkspaceCollectionService.COLLECTION_NAME, {
            name: WorkspaceCollectionService.INDEX_NAME,
            fields: [ Workspace.NAME_FIELD_NAME, Workspace.OWNER_FIELD_NAME ],
            unique: true,
            type: "hash"
        });

        LOGGER.info("[Workspace-Collection] Create " + WorkspaceCollectionService.COLLECTION_NAME + " index");
    }
};

/**
 * Create service
 *
 * @return {Promise<WorkspaceCollectionService>} Workspace service
 */
const create = async () => {
    const service = new WorkspaceCollectionService(await createService(WorkspaceCollectionService.COLLECTION_NAME, UserCollectionService.COLLECTION_NAME));

    // Create edge collection
    service.service.collections[WorkspaceCollectionService.LINK_COLLECTION_NAME] = await edgeCollection(WorkspaceCollectionService.LINK_COLLECTION_NAME);
    service.service.collections[ProjectCollectionService.LINK_COLLECTION_NAME] = await edgeCollection(ProjectCollectionService.LINK_COLLECTION_NAME);

    // Create index
    await createIndex(service);

    return service;
};

export { create, WorkspaceCollectionService };