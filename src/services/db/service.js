import { aql } from "arangojs";
import { DB } from "../../common/errors";
import ServerError from "../../common/servererror";
import { database } from "../../config/database";
import { collection } from "../../utils/database";

/**
 * Basic service for database querying
 */
class BasicDBService {

    /**
     * Constructor
     */
    constructor() {
        this.collections = {};
    }

    /**
     * Set collections
     *
     * @param collections Collections
     */
    set collections(collections) {
        this._collections = collections;
    }

    /**
     * Get collections
     *
     * @return {Array<DocumentCollection|EdgeCollection>} Collections
     */
    get collections() {
        return this._collections;
    }

    /**
     * Get collection with its name
     *
     * @param name Collection's name
     * @return {DocumentCollection|EdgeCollection} Collection
     */
    collection(name) {
        if (this.collections[name] === undefined) {
            throw new UnknownCollection(name);
        }
        return this.collections[name];
    }

    /**
     * Find documents with a query
     *
     * @param query Query
     * @return {Promise<any>} Documents
     */
    async query(query) {
        return (await database.query(query)).all();
    }

    /**
     * Find a document with its key
     *
     * @param collection Collection's name
     * @param key Document's key
     * @return {Promise<any|Document<any>>} Document
     */
    findByKey(collection, key) {
        return this.collection(collection).document(key);
    }

    /**
     * Find document with field matching or not value
     *
     * @param collection Collection's name
     * @param field Field
     * @param value Value
     * @param equals Field's value must match value
     * @return {Promise<any>} Documents
     */
    async findBy(collection, field, value, equals = true) {
        return (await database.query(aql`
            FOR doc in ${this.collection(collection)}
                FILTER ${aql.literal(createIntermediaryChecks("doc", field))} ${aql.literal("doc." + field)} ${equals ? aql.literal("==") : aql.literal("!=")} ${value}
                RETURN doc`)
        ).all();
    }

    /**
     * Get all documents
     *
     * @param collection Collection's name
     * @return {Promise<any>} Documents
     */
    async all(collection) {
        return (await database.query(aql`
            FOR doc in ${this.collection(collection)}
                RETURN doc`)
        ).all();
    }

    /**
     * Replace a document by an other
     *
     * @param collection Collection's name
     * @param key Document's key
     * @param document Document
     * @return {Promise<any>} Promise
     */
    replace(collection, key, document) {
        return this.collection(collection).replace(key, document);
    }

    /**
     * Update a document with a merging mechanism
     *
     * @param collection Collection's name
     * @param key Document's key
     * @param document Document
     * @param options Options
     * @return {Promise<any>} Promise
     */
    update(collection, key, document, options = { returnOld: true, returnNew: true }) {
        return this.collection(collection).update(key, document, options);
    }

    /**
     * Remove a document
     *
     * @param collection Collection's name
     * @param key Document's key
     * @return {Promise<any>} Promise
     */
    remove(collection, key) {
        return this.collection(collection).remove(key);
    }

    /**
     * Test whether a document exists
     *
     * @param collection Collection's name
     * @param key Document's key
     * @return {Promise<boolean>} Promise
     */
    exists(collection, key) {
        return this.collection(collection).documentExists(key);
    }

    /**
     * Save a list of documents
     *
     * @param collection Collection's name
     * @param documents Documents
     * @return {Promise<any[]>} Info for each document
     */
    save(collection, ...documents) {
        return this.collection(collection).save([ ...documents ]);
    }

    /**
     * Get index with a find function
     *
     * @param collection Collection's name
     * @param predicate Predicate
     * @return {Promise<*>} Index or undefined
     */
    async indexBy(collection, predicate) {
        return (await this.collection(collection).indexes()).find(predicate);
    }

    /**
     * Create an index
     *
     * @param collection Collection's name
     * @param index Index
     */
    async createIndex(collection, index) {
        await this.collection(collection).createIndex(index);
    }

    /**
     * Execute a transaction
     *
     * @param actions List of functions (no async function) to execute
     * @param collections Collections parameter of https://www.arangodb.com/docs/stable/drivers/js-reference-database-transactions.html#databasebegintransaction
     * @param options Option parameter of https://www.arangodb.com/docs/stable/drivers/js-reference-database-transactions.html#databasebegintransaction
     * @return {Promise<void>} Promise
     */
    async executeTransaction(actions, collections, options) {
        const transaction = await database.beginTransaction(collections, options);

        try {
            let results = [];

            // Run actions
            for (let action of actions) {
                const result = await transaction.run(() => action(results));
                results.push(result);

                if (result instanceof Array) {
                    result.filter(r => r instanceof Object && r.error && r.errorMessage).forEach(r => {
                        throw new ServerError(r.errorMessage);
                    });
                }
            }

            // Commit changes
            await transaction.commit();
        } catch (e) {
            // Abort transaction
            await transaction.abort();

            throw e;
        }
    }
}

/**
 * Error when an unknown collection is encountered
 */
class UnknownCollection extends ServerError {

    /**
     * Constructor
     *
     * @param collection
     */
    constructor(collection) {
        super(`${DB.UNKNOWN_COLLECTION}_${collection.toUpperCase()}`, 500);
    }
}

/**
 * Attach collections to a service
 *
 * @param service Service
 * @param collections Collections
 */
const attachCollections = async (service, ...collections) => {
    for (const col of collections) {
        service.collections[col] = await collection(col);
    }
};

/**
 * Create intermediary checks in a FILTER
 *
 * @param name Object's name
 * @param accessor Target accessor
 * @return {string} Checks
 */
const createIntermediaryChecks = (name, accessor) => {
    const parts = accessor.split(".").slice(0, -1);

    // Check parts length
    if (parts.length === 0) {
        return "";
    }

    let checks = "";
    for (let i = 0; i < parts.length; i++) {
        checks += name + "." + parts.slice(0, i + 1).join(".") + " != null &&";
    }
    return checks;
};

/**
 * Create a basic database service
 *
 * @param collections Collections required
 * @return {Promise<BasicDBService>} Service
 */
const create = async (...collections) => {
    const service = new BasicDBService();

    // Attach collections
    await attachCollections(service, ...collections);

    return service;
};

export { create, UnknownCollection };