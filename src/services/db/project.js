import { aql } from "arangojs";
import { database } from "../../config/database";
import { Project } from "../../model/project";
import { LOGGER } from "../../utils";
import { edgeCollection } from "../../utils/database";
import { ProjectService } from "../project";
import { create as createService } from "./service";
import { WorkspaceCollectionService } from "./workspace";

class ProjectCollectionService {

    /**
     * Constructor
     *
     * @param service Database service
     */
    constructor(service) {
        this.service = service;
    }

    /**
     * Get service
     *
     * @return {BasicDBService} Database service
     */
    get service() {
        return this._service;
    }

    /**
     * Set service
     *
     * @param service Database service
     */
    set service(service) {
        this._service = service;
    }

    /**
     * Save projects
     *
     * @param projects Projects
     */
    async save(...projects) {
        const objects = [ ...projects ];
        objects.forEach(Project.Check);

        // Save
        const info = await this.service.save(ProjectCollectionService.COLLECTION_NAME, ...projects);

        // Set key
        for (let i = 0; i < info.length; i++) {
            objects[i].key = info[i]._key;

            LOGGER.debug(`[Project-Collection] Save ${objects[i].key}/${objects[i].workspace.split("/")[1]}/${objects[i].name}`);
        }
    }

    /**
     * Find a project
     *
     * @param workspace Workspace
     * @param name Project's name
     * @return {Promise<Project>} Project or undefined
     */
    async find(workspace, name) {
        let projects = await (await database.query(aql`
            FOR project in ${this.service.collection(ProjectCollectionService.COLLECTION_NAME)}
                FILTER project.workspace == ${WorkspaceCollectionService.Id(workspace)} AND project.name == ${name}
                
                RETURN project
                `)
        ).all();

        return projects.map(Project.FromObject)[0];
    }

    /**
     * Update a project
     *
     * @param key Project's key
     * @param project Project
     */
    async update(key, project) {
        Project.Check(project, false);

        await this.service.executeTransaction([
            () => this.service.update(ProjectCollectionService.COLLECTION_NAME, key, project)
        ], { write: ProjectCollectionService.COLLECTION_NAME });
    }

    /**
     * Delete projects related to a workspace
     *
     * @param workspace Workspace
     * @return {Promise<void>}
     */
    async deleteFromWorkspace(workspace) {
        // Get projects
        let projects = await (await database.query(aql`
            FOR project in ${this.service.collection(ProjectCollectionService.COLLECTION_NAME)}
                FILTER project.workspace == ${WorkspaceCollectionService.Id(workspace)}
                
                RETURN project`)
        ).all();

        // Get links
        let links = await (await database.query(aql`
            FOR link in ${this.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME)}
                FILTER link._to IN ${projects.map(p => p._id)}
                
                RETURN link._key`)
        ).all();

        // Delete
        await this.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME).removeByKeys(links);
        await this.service.collection(ProjectCollectionService.COLLECTION_NAME).removeByKeys(projects.map(p => p._key));
    }

    /**
     * Delete a project
     *
     * @param project Project
     * @return {Promise<void>}
     */
    async delete(project) {
        // Get links
        let links = await (await database.query(aql`
            FOR link in ${this.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME)}
                FILTER link._to == ${ProjectService.Id(project)}
                
                RETURN link._key`)
        ).all();

        // Delete
        await this.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME).removeByKeys(links);
        await this.service.collection(ProjectCollectionService.COLLECTION_NAME).remove(project.key);
    }

    /**
     * Get project's id
     *
     * @param project Project
     */
    static Id(project) {
        return ProjectCollectionService.COLLECTION_NAME + "/" + project.key;
    }

    /**
     * Convert an object into a project with a check
     *
     * @param object Object
     * @return {Project} Project
     */
    static ToWorkspace(object) {
        const project = Project.FromObject(object);

        Project.Check(project);
        return project;
    }

    static get COLLECTION_NAME() {
        return "projects";
    }

    static get LINK_COLLECTION_NAME() {
        return "workspaces_to_projects";
    }

    static get INDEX_NAME() {
        return "projects-index";
    }
}

/**
 * Create index
 *
 * @param service
 */
const createIndex = async (service) => {
    const index_col = await service.service.indexBy(ProjectCollectionService.COLLECTION_NAME, index => index.name === ProjectCollectionService.INDEX_NAME);

    // Create indices
    if (index_col === undefined) {
        await service.service.createIndex(ProjectCollectionService.COLLECTION_NAME, {
            name: ProjectCollectionService.INDEX_NAME,
            fields: [ Project.NAME_FIELD_NAME, Project.WORKSPACE_FIELD_NAME ],
            unique: true,
            type: "hash"
        });

        LOGGER.info("[Workspace-Collection] Create " + ProjectCollectionService.COLLECTION_NAME + " index");
    }
};

/**
 * Create service
 *
 * @return {Promise<ProjectCollectionService>} Project service
 */
const create = async () => {
    const service = new ProjectCollectionService(await createService(ProjectCollectionService.COLLECTION_NAME, ProjectCollectionService.COLLECTION_NAME));

    // Create edge collection
    service.service.collections[ProjectCollectionService.LINK_COLLECTION_NAME] = await edgeCollection(ProjectCollectionService.LINK_COLLECTION_NAME);

    // Create index
    await createIndex(service);

    return service;
};

export { create, ProjectCollectionService };