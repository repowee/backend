import { Project } from "../model/project";
import { ProjectCollectionService } from "./db/project";
import { UserCollectionService } from "./db/user";
import { WorkspaceCollectionService } from "./db/workspace";
import { UserService } from "./user";

/**
 * Project service
 */
class ProjectService {

    /**
     * Constructor
     *
     * @param {DBServiceContainer} dbContainer Database services
     * @param {DockerService} docker Docker service
     */
    constructor(dbContainer, docker) {
        this.dbService = dbContainer;
        this.docker = docker;
    }

    /**
     * Create a project
     *
     * @param name Name
     * @param workspace Workspace
     * @param creator Creator
     * @return {Promise<void>}
     */
    async create(name, workspace, creator) {
        const project = new Project(name, UserCollectionService.Id(creator), WorkspaceCollectionService.Id(workspace));

        // Save project
        await this.dbService.project.save(project);

        // Link project and workspaces
        await this.dbService.project.service.collection(ProjectCollectionService.LINK_COLLECTION_NAME).save({
            _from: WorkspaceCollectionService.Id(workspace),
            _to: ProjectCollectionService.Id(project)
        });
    }

    /**
     * Find a project
     *
     * @param workspace Workspace
     * @param name Project's name
     * @return {Promise<Project>} Project or undefined
     */
    async find(workspace, name) {
        return this.dbService.project.find(workspace, name);
    }

    /**
     * Update a given project
     *
     * @param project Project
     * @param new_project Project new data
     * @return {Promise<void>}
     */
    async update(project, new_project) {
        const update_project = new Project();

        // Update name
        if (new_project.name !== undefined) {
            update_project.name = new_project.name;
        }
        if (new_project.image !== undefined) {
            update_project.image = new_project.image;
        }
        if (new_project.container !== undefined) {
            update_project.container = new_project.container;
        }
        if (new_project.ports !== undefined) {
            update_project.ports = new_project.ports;
        }

        await this.dbService.project.update(project.key, update_project);
    }

    /**
     * Delete a project
     *
     * @param project Project
     * @return {Promise<void>}
     */
    async delete(project) {
        await this.dbService.project.delete(project);
    }

    /**
     * Get project's id
     *
     * @param project Project
     */
    static Id(project) {
        return ProjectCollectionService.COLLECTION_NAME + "/" + project.key;
    }

    /**
     * Transform a project into an API object
     *
     * @param project Project
     * @param creator Creator
     * @return {Promise<Object>} API object
     */
    static ToAPIObject(project, creator) {
        const object = project.toJSON();

        object.status = object.image && object.container ? "DEPLOYED" : (object.image ? "BUILT" : "INITIALIZED");
        object.creator = UserService.ToAPIObject(creator);
        
        // Delete sensitive fields
        delete object.workspace;
        delete object.image;
        delete object.container;

        return object;
    }
}

/**
 * Create project service
 *
 * @param {DBServiceContainer} dbContainer Database services
 * @param {DockerService} docker Docker service
 * @return {ProjectService} Service
 */
const create = (dbContainer, docker) => {
    return new ProjectService(dbContainer, docker);
};

export { create, ProjectService };