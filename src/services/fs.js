import fs from "fs";
import { server as serverConfig } from "../config/config";

class FileSystemService {

    /**
     * Clean-up temporary directory
     */
    cleanUpTmpDirectory() {
        fs.rmdirSync(serverConfig.tmpDir, {
            recursive: true
        });
    }
}

/**
 * Create filesystem service
 *
 * @return {FileSystemService} Service
 */
const create = () => {
    return new FileSystemService();
};

export { create, FileSystemService };