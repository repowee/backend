import { CronJob } from "cron";
import moment from "moment";
import dockerConfig from "../config/docker";
import { LOGGER } from "../utils";
import { DateUtilities } from "../utils/database";
import { environment } from "../utils/node";

const JOBS = [];

/**
 * Start a cron job
 *
 * @param tick Tick
 * @param callback Callback
 */
const startJob = (tick, callback) => {
    JOBS.push(new CronJob(tick, callback, undefined, true));
    callback();
};

/**
 * Clean-up unused docker networks
 *
 * @param {ServiceContainer} services Service container
 */
const cleanDockerNetworks = async (services) => {
    // Get all networks & workspaces
    let [ workspaces, networks ] = await Promise.all([ services.db.workspace.all(), services.docker.networks() ]);

    // Extract only ids
    workspaces = workspaces.map(w => w.network);

    // Get unused networks
    const networks_promises = [];
    for (const network of networks.filter(n => !workspaces.includes(n.Id) && moment().diff(moment(n.Labels.created, DateUtilities.FORMAT), "minutes") > 10)) {
        networks_promises.push(services.docker.network(network.Id));
    }
    const unused_networks = await Promise.all(networks_promises);

    // Remove unused networks
    for (let i = 0; i < unused_networks.length; i++) {
        networks_promises[i] = unused_networks[i].remove();
    }
    await Promise.all(networks_promises);
    LOGGER.info(`[SCH] Removed unused docker networks (${networks_promises.length})`);
};

/**
 * Clean-up unused docker images
 *
 * @param {ServiceContainer} services Service container
 */
const cleanDockerImages = async (services) => {
    await new Promise(((resolve, reject) => {
        services.docker.docker.pruneImages({ filters: { dangling: { true: true }, until: { "10m": true } } }, (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    }));

    LOGGER.info("[SCH] Remove unused images");
};

/**
 * Clean-up unused docker containers
 *
 * @param {ServiceContainer} services Service container
 */
const cleanDockerContainers = async (services) => {
    await new Promise(((resolve, reject) => {
        services.docker.docker.pruneContainers({ filters: { until: { "10m": true } } }, (error, data) => {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    }));

    LOGGER.info("[SCH] Remove unused containers");
};

/**
 * Start scheduler tasks
 *
 * @param {ServiceContainer} services Service container
 */
const start = (services) => {
    // Start docker tasks
    if (environment() !== "ci") {
        startJob(dockerConfig.schedulerTick, () => cleanDockerNetworks(services));
        startJob(dockerConfig.schedulerTick, () => cleanDockerImages(services));
        startJob(dockerConfig.schedulerTick, () => cleanDockerContainers(services));
    }
};

export { start, JOBS };