import { genSalt as saltGenerator, hash as bcryptHash } from "bcrypt";

const SALT_ROUNDS = 10;

/**
 * Function to hash a string with bcrypt algorithm
 *
 * @param string String
 * @param salt Salt
 * @return {Promise<string>} Hash
 */
const hash = async (string, salt) => {
    return await bcryptHash(string, salt);
};

/**
 * Function to generate a salt
 *
 * @return {Promise<string>} Salt
 */
const genSalt = async () => {
    return saltGenerator(SALT_ROUNDS);
};

/**
 * Test whether a string is a bcrypt hash
 *
 * @param string String
 * @return {boolean} true if string is a hash, false otherwise
 */
const isHash = (string) => {
    return typeof string === "string" && string.startsWith(`$2b$${SALT_ROUNDS}$`) && string.length === 60;
};

export { hash, genSalt, isHash };