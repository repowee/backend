import moment from "moment";
import { config, database } from "../config/database";
import LOGGER from "./logger";

/**
 * Get collection (create if collection doesn't exist)
 *
 * @param name Collection's name
 * @return {Promise<DocumentCollection<any>>} Collection
 */
const collection = async (name) => {
    const col = database.collection(name);

    if (!await collectionExists(name)) {
        // Create collection
        await col.create();

        // Load collection
        let loaded = false;
        if (config.memory instanceof Object && config.memory.collections instanceof Array && config.memory.collections.indexOf(name) !== -1) {
            await col.load(false);
            loaded = true;
        }
        LOGGER.info("[Database] Create collection: " + name + " (loaded: " + loaded + ")");
    }
    return col;
};

/**
 * Get edge collection (create if collection doesn't exist)
 *
 * @param name Collection's name
 * @return {Promise<DocumentCollection<any>>} Collection
 */
const edgeCollection = async (name) => {
    const col = database.edgeCollection(name);

    if (!await collectionExists(name)) {
        // Create collection
        await col.create();

        // Load collection
        let loaded = false;
        if (config.memory instanceof Object && config.memory.collections instanceof Array && config.memory.collections.indexOf(name) !== -1) {
            await col.load(false);
            loaded = true;
        }
        LOGGER.info("[Database] Create edge collection: " + name + " (loaded: " + loaded + ")");
    }
    return col;
};

/**
 * Test whether a collection exists
 *
 * @param name Collection's name
 * @return {Promise<boolean>} true if collection exists, false otherwise
 */
const collectionExists = async (name) => {
    return (await database.listCollections()).find((collection) => collection.name === name) !== undefined;
};

/**
 * An utility class for dates
 */
class DateUtilities {

    /**
     * Date format
     *
     * @return {string} Format
     */
    static get FORMAT() {
        return moment.HTML5_FMT.DATETIME_LOCAL_MS;
    }

    /**
     * Convert a date into a string with the valid format
     *
     * @param date Date
     * @return {string} String
     */
    static ToString(date) {
        return date.format(DateUtilities.FORMAT);
    }

    /**
     * Convert a string into a date
     *
     * @param string String
     * @return {moment.Moment} Date
     */
    static FromString(string) {
        return moment(string, DateUtilities.FORMAT);
    }
}

/**
 * Enumeration of Arango error codes (https://www.arangodb.com/docs/stable/appendix-error-codes.html)
 */
class ArangoErrorCode {

    static get UNIQUE_CONSTRAINT_VIOLATED() {
        return 1210;
    }

    static get DOCUMENT_NOT_FOUND() {
        return 1202;
    }
}

export { collection, edgeCollection, collectionExists, DateUtilities, ArangoErrorCode };