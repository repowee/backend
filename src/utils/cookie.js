import moment from "moment";
import ms from "ms";
import serverConfig from "../config/server";

/**
 * Get the expiration date of a cookie
 *
 * @return {Date} Date
 */
const expirationDate = () => {
    return moment().add(ms(serverConfig.cookieDuration), "ms").toDate();
};

export { expirationDate };