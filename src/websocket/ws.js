import fs from "fs";
import AuthError from "../common/autherror";
import { AUTH } from "../common/errors";
import { LOGGER } from "../utils";

const project = JSON.parse(fs.readFileSync("./package.json", "UTF-8"));
const CONNECTION_MESSAGE = "CONNECTION_OPENED_WITH_REPOWEE_" + project.version;

const console_register = {};

/**
 * Function to init socket (implement auth, set-up callbacks...)
 *
 * @param socket Socket
 * @param services Services
 */
const init = (socket, services) => {
    LOGGER.info("Open a websocket on localhost" + socket._path);

    // Define authentication middleware
    socket.use(async (socket, next) => {
        // Check token in request
        if (socket.handshake.query === undefined || socket.handshake.query.token === undefined) {
            return next(new AuthError(AUTH.TOKEN_UNDEFINED));
        }

        // Verify token
        try {
            await services.auth.verifyToken(socket.handshake.query.token);
            next();
        } catch (e) {
            next(e);
        }
    });

    socket.on("connect", client => {
        LOGGER.info("[WS] Connection opened with " + client.handshake.address);

        client.emit("connection", CONNECTION_MESSAGE);

        client.on("console:register", data => {
            console_register[data] = client;
        });

        client.on("disconnect", () => {
            LOGGER.info("[WS] Connection closed with " + client.handshake.address);
        });
    });
};

export { init, CONNECTION_MESSAGE, console_register };