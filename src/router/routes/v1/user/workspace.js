import { HTTP, USER_API, WORKSPACE_API } from "../../../../common/errors";
import ServerError from "../../../../common/servererror";
import { User, UserRole } from "../../../../model/user";
import { WorkspaceService } from "../../../../services/workspace";
import { AuthRoute, URL_SELF_KEYWORD } from "../../../route";
import { Response } from "../../../router";

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const create = async (services, request, response, next) => {
    try {
        // Check mail parameter & body
        if (typeof request.param("mail") !== "string") {
            throw new ServerError(USER_API.MAIL_UNDEFINED);
        }
        if (!(request.body instanceof Object)) {
            throw new Error(HTTP.BODY_UNDEFINED);
        }

        // Find user
        const user = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Check user
        if (current_user.mail !== request.param("mail") && request.param("mail") !== URL_SELF_KEYWORD && current_user.role !== UserRole.ADMIN) {
            throw new ServerError(WORKSPACE_API.DIFFERENT_USER);
        }

        // Create workspace
        await services.workspace.create(user, request.body);
        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /list
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const list = async (services, request, response, next) => {
    try {
        // Get current user
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);
        if (!(current_user instanceof User)) {
            throw new ServerError(USER_API.INVALID_USER);
        }

        // Find & check user
        const user = await services.user.findById(request, request.param("mail"));
        if (!(user instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }
        if (current_user.mail !== user.mail && current_user.role !== UserRole.ADMIN) {
            throw new ServerError(WORKSPACE_API.DIFFERENT_USER);
        }

        response.json(new Response((await services.workspace.list(user)).map(w => WorkspaceService.ToAPIObject(w, w.real_owner, w.users, w.projects))));
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const get = async (services, request, response, next) => {
    try {
        // Get current user
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);
        if (!(current_user instanceof User)) {
            throw new ServerError(USER_API.INVALID_USER);
        }

        // Find owner
        const owner = await services.user.findById(request, request.param("mail"));
        if (!(owner instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("name"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        response.json(new Response(WorkspaceService.ToAPIObject(workspace, owner, workspace.users, workspace.projects, workspace.network)));
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const update = async (services, request, response, next) => {
    try {
        // Check body
        if (!(request.body instanceof Object)) {
            throw new Error(HTTP.BODY_UNDEFINED);
        }

        // Get current user
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);
        if (!(current_user instanceof User)) {
            throw new ServerError(USER_API.INVALID_USER);
        }

        // Find owner
        const owner = await services.user.findById(request, request.param("mail"));
        if (!(owner instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Check user
        if (current_user.mail !== owner.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("name"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Update workspace
        await services.workspace.update(workspace, request.body);

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /invite
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const invite = async (services, request, response, next) => {
    try {
        // Check parameter
        if (request.param("user") === undefined) {
            throw new ServerError(WORKSPACE_API.UNDEFINED_USER);
        }

        // Get current user
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);
        if (!(current_user instanceof User)) {
            throw new ServerError(USER_API.INVALID_USER);
        }

        // Find owner
        const owner = await services.user.findById(request, request.param("mail"));
        if (!(owner instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Check user rights
        if (current_user.mail !== owner.mail && current_user.role !== UserRole.ADMIN) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find given user
        const invited_user = await services.user.findById(request, request.param("user"));
        if (invited_user === undefined) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Check user
        if (current_user.mail === invited_user.mail) {
            throw new ServerError(WORKSPACE_API.CANT_INVITE_OWNER);
        }

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("name"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Add user
        await services.workspace.invite(workspace, invited_user);

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /kick
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const kick = async (services, request, response, next) => {
    try {
        // Check parameter
        if (request.param("user") === undefined) {
            throw new ServerError(WORKSPACE_API.UNDEFINED_USER);
        }

        // Get current user
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);
        if (!(current_user instanceof User)) {
            throw new ServerError(USER_API.INVALID_USER);
        }

        // Find owner
        const owner = await services.user.findById(request, request.param("mail"));
        if (!(owner instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Check user rights
        if (current_user.mail !== owner.mail && current_user.role !== UserRole.ADMIN) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find given user
        const invited_user = await services.user.findById(request, request.param("user"));
        if (invited_user === undefined) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Check user
        if (current_user.mail === invited_user.mail) {
            throw new ServerError(WORKSPACE_API.CANT_INVITE_OWNER);
        }

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("name"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Kick user
        await services.workspace.kick(workspace, invited_user);

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const _delete = async (services, request, response, next) => {
    try {
        // Get current user
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);
        if (!(current_user instanceof User)) {
            throw new ServerError(USER_API.INVALID_USER);
        }

        // Find owner
        const owner = await services.user.findById(request, request.param("mail"));
        if (!(owner instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Check user
        if (current_user.mail !== owner.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("name"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        await services.workspace.delete(workspace);

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

export default {
    "workspace-creation": new AuthRoute(route => route + "/:mail/workspace", "post", create),
    "workspace-list": new AuthRoute(route => route + "/:mail/workspace/list", "get", list),
    "workspace-get": new AuthRoute(route => route + "/:mail/workspace/:name", "get", get),
    "workspace-delete": new AuthRoute(route => route + "/:mail/workspace/:name", "delete", _delete),
    "workspace-update": new AuthRoute(route => route + "/:mail/workspace/:name", "patch", update),
    "workspace-invite": new AuthRoute(route => route + "/:mail/workspace/:name/invite", "post", invite),
    "workspace-kick": new AuthRoute(route => route + "/:mail/workspace/:name/kick", "post", kick)
};