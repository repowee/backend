import extract from "extract-zip";
import getPort from "get-port";
import { resolve } from "path";
import { HTTP, PROJECT_API, USER_API, WORKSPACE_API } from "../../../../common/errors";
import ServerError from "../../../../common/servererror";
import dockerConfig from "../../../../config/docker";
import { ProjectService } from "../../../../services/project";
import { console_register } from "../../../../websocket/ws";
import { AuthRoute, URL_SELF_KEYWORD } from "../../../route";
import { Response } from "../../../router";

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const create = async (services, request, response, next) => {
    try {
        // Check mail parameter & body
        if (typeof request.param("mail") !== "string") {
            throw new ServerError(WORKSPACE_API.MAIL_UNDEFINED);
        }
        if (typeof request.param("workspace") !== "string") {
            throw new ServerError(PROJECT_API.WORKSPACE_UNDEFINED);
        }
        if (typeof request.param("name") !== "string") {
            throw new ServerError(PROJECT_API.NAME_UNDEFINED);
        }
        if (!(request.body instanceof Object)) {
            throw new Error(HTTP.BODY_UNDEFINED);
        }

        // Find users
        const owner = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("workspace"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Create project
        await services.project.create(request.param("name"), workspace, current_user);
        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const get = async (services, request, response, next) => {
    try {
        // Check mail parameter & body
        if (typeof request.param("mail") !== "string") {
            throw new ServerError(WORKSPACE_API.MAIL_UNDEFINED);
        }
        if (typeof request.param("workspace") !== "string") {
            throw new ServerError(PROJECT_API.WORKSPACE_UNDEFINED);
        }
        if (typeof request.param("name") !== "string") {
            throw new ServerError(PROJECT_API.NAME_UNDEFINED);
        }

        // Find users
        const owner = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("workspace"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find project
        const project = await services.project.find(workspace, request.param("name"));
        if (project === undefined) {
            throw new ServerError(PROJECT_API.PROJECT_NOT_FOUND, 404);
        }

        // Find creator
        const creator = await services.project.dbService.user.findByKey(project.creator.split("/")[1]);
        if (creator === undefined) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        response.json(new Response(ProjectService.ToAPIObject(project, creator)));
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const update = async (services, request, response, next) => {
    try {
        if (!(request.body instanceof Object)) {
            throw new Error(HTTP.BODY_UNDEFINED);
        }

        // Find users
        const owner = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("workspace"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find project
        const project = await services.project.find(workspace, request.param("name"));
        if (project === undefined) {
            throw new ServerError(PROJECT_API.PROJECT_NOT_FOUND, 404);
        }

        // Update project
        await services.project.update(project, request.body);

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const _delete = async (services, request, response, next) => {
    try {
        // Find users
        const owner = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("workspace"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find project
        const project = await services.project.find(workspace, request.param("name"));
        if (project === undefined) {
            throw new ServerError(PROJECT_API.PROJECT_NOT_FOUND, 404);
        }

        // Delete project
        await services.project.delete(project);

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /build
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const build = async (services, request, response, next) => {
    try {
        // Check data
        if (!(request.files instanceof Object) || !(request.files.data instanceof Object)) {
            throw new ServerError(HTTP.DATA_UNDEFINED);
        }

        // Find users
        const owner = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("workspace"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find project
        const project = await services.project.find(workspace, request.param("name"));
        if (project === undefined) {
            throw new ServerError(PROJECT_API.PROJECT_NOT_FOUND, 404);
        }

        // Get socket
        const socket = console_register[`${owner.mail}-${workspace.name}-${project.name}`];

        // Extract project
        const abs_project_path = resolve(request.files.data.tempFilePath) + "_extracted/";
        await extract(resolve(request.files.data.tempFilePath), { dir: abs_project_path });

        // Build project
        const image_and_ports = await services.docker.buildImageFromFolder(abs_project_path, socket);

        // Allocate ports
        for (let i = 0; i < image_and_ports.ports.length; i++) {
            image_and_ports.ports[i] = {
                from: image_and_ports.ports[i],
                to: await getPort({ port: getPort.makeRange(dockerConfig.portRangeStart, dockerConfig.portRangeEnd) })
            };
        }

        // Save image
        await services.project.update(project, {
            image: image_and_ports.image,
            container: null,
            ports: image_and_ports.ports
        });

        // Share new info
        if (socket) {
            socket.emit("ports:change", image_and_ports.ports);
            socket.emit("project-status:change", "BUILT");
        }

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /start
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const start = async (services, request, response, next) => {
    try {
        // Find users
        const owner = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("workspace"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find project
        const project = await services.project.find(workspace, request.param("name"));
        if (project === undefined) {
            throw new ServerError(PROJECT_API.PROJECT_NOT_FOUND, 404);
        }

        // Get socket
        const socket = console_register[`${owner.mail}-${workspace.name}-${project.name}`];

        if (typeof project.container === "string") { // Start container
            await services.docker.startContainer(project.container, socket);
        } else { // Create and start container
            const id = await services.docker.runContainer(project.image, project.ports, socket);

            // Connect to network
            const network = await services.docker.network(workspace.real_network);
            await network.connect({
                Container: id
            });

            // Save container's id
            await services.project.update(project, {
                container: id,
            });

            // Share new info
            if (socket) {
                socket.emit("project-status:change", "DEPLOYED");
            }
        }

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /stop
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const stop = async (services, request, response, next) => {
    try {
        // Find users
        const owner = await services.user.findById(request, request.param("mail"));
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);

        // Find workspace
        const workspace = await services.workspace.find(owner, request.param("workspace"));
        if (workspace === undefined) {
            throw new ServerError(WORKSPACE_API.WORKSPACE_NOT_FOUND, 404);
        }

        // Check user rights
        if (workspace.users.filter(u => u.mail === current_user.mail).length === 0 && owner.mail !== current_user.mail) {
            throw new ServerError(WORKSPACE_API.UNAUTHORIZED_USER);
        }

        // Find project
        const project = await services.project.find(workspace, request.param("name"));
        if (project === undefined) {
            throw new ServerError(PROJECT_API.PROJECT_NOT_FOUND, 404);
        }

        // Stop container
        if (typeof project.container === "string") {
            await services.docker.stopContainer(project.container);
        }

        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

export default {
    "project-creation": new AuthRoute(route => route + "/:mail/workspace/:workspace/project", "post", create),
    "project-get": new AuthRoute(route => route + "/:mail/workspace/:workspace/project/:name", "get", get),
    "project-update": new AuthRoute(route => route + "/:mail/workspace/:workspace/project/:name", "patch", update),
    "project-delete": new AuthRoute(route => route + "/:mail/workspace/:workspace/project/:name", "delete", _delete),
    "project-build": new AuthRoute(route => route + "/:mail/workspace/:workspace/project/:name/build", "post", build),
    "project-start": new AuthRoute(route => route + "/:mail/workspace/:workspace/project/:name/start", "post", start),
    "project-stop": new AuthRoute(route => route + "/:mail/workspace/:workspace/project/:name/stop", "post", stop),
};