import { HTTP, USER_API } from "../../../../common/errors";
import ServerError from "../../../../common/servererror";
import { User, UserRole } from "../../../../model/user";
import { UserService } from "../../../../services/user";
import { AdminRoute, AuthRoute, URL_SELF_KEYWORD } from "../../../route";
import { Response } from "../../../router";

/**
 * Function that defines a callback on /
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const create = async (services, request, response, next) => {
    try {
        if (!(request.body instanceof Object)) {
            throw new Error(HTTP.BODY_UNDEFINED);
        }

        // Create user
        await services.user.create(request.body);
        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /list
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const list = async (services, request, response, next) => {
    try {
        response.json(new Response((await services.user.all()).map(UserService.ToAPIObject)));
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /:mail
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const get = async (services, request, response, next) => {
    try {
        // Check mail parameter
        if (typeof request.param("mail") !== "string") {
            throw new ServerError(USER_API.MAIL_UNDEFINED);
        }

        // Find user
        const user = await services.user.findById(request, request.param("mail"));

        // Send response
        if (!(user instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }
        response.json(new Response(UserService.ToAPIObject(user)));
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /:mail
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const _delete = async (services, request, response, next) => {
    try {
        // Check mail parameter
        if (typeof request.param("mail") !== "string") {
            throw new ServerError(USER_API.MAIL_UNDEFINED);
        }

        // Find & check user
        const user = await services.user.findById(request, request.param("mail"));
        if (!(user instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }

        // Remove user
        await services.user.remove(user);

        // Send response
        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /:mail
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const update = async (services, request, response, next) => {
    try {
        // Check mail parameter & body
        if (typeof request.param("mail") !== "string") {
            throw new ServerError(USER_API.MAIL_UNDEFINED);
        }
        if (!(request.body instanceof Object)) {
            throw new Error(HTTP.BODY_UNDEFINED);
        }

        // Get current user
        const current_user = await services.user.findById(request, URL_SELF_KEYWORD);
        if (!(current_user instanceof User)) {
            throw new ServerError(USER_API.INVALID_USER);
        }

        // Find & check user
        const user = await services.user.findById(request, request.param("mail"));
        if (!(user instanceof User)) {
            throw new ServerError(USER_API.USER_NOT_FOUND, 404);
        }
        if (current_user.mail !== user.mail && current_user.role !== UserRole.ADMIN) {
            throw new ServerError(USER_API.UNAUTHORIZED_USER);
        }

        // Update user
        await services.user.update(user, request.body, current_user.role === UserRole.ADMIN);

        // Send response
        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /search?query=
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const search = async (services, request, response, next) => {
    try {
        // Check query parameter
        if (request.param("query") === undefined) {
            throw new ServerError(USER_API.QUERY_UNDEFINED);
        }

        // Search users
        const users = await services.user.searchByMail(request.param("query"));

        response.json(new Response(users.map(UserService.ToAPIObject)));
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /batch
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const batch = async (services, request, response, next) => {
    try {
        // Check data
        if (!(request.files instanceof Object) || !(request.files.data instanceof Object)) {
            throw new ServerError(HTTP.DATA_UNDEFINED);
        }

        // Create users
        await services.user.createFromCSV(request.files.data.tempFilePath);
        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

export default {
    "user-creation": new AdminRoute(route => route, "post", create),
    "user-list": new AuthRoute(route => route + "/list", "get", list),
    "user-search": new AuthRoute(route => route + "/search", "get", search),
    "user-batch": new AdminRoute(route => route + "/batch", "post", batch),
    "user-get": new AuthRoute(route => route + "/:mail", "get", get),
    "user-delete": new AuthRoute(route => route + "/:mail", "delete", _delete),
    "user-update": new AuthRoute(route => route + "/:mail", "patch", update)
};