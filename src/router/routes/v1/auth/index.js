import { encode as encodeBase64 } from "base64-url";
import { HTTP } from "../../../../common/errors";
import { expirationDate } from "../../../../utils/cookie";
import { AuthRoute, Route } from "../../../route";
import { Response } from "../../../router";

/**
 * Function that defines a callback on /login requests
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const login = async (services, request, response, next) => {
    try {
        if (!(request.body instanceof Object)) {
            throw new Error(HTTP.BODY_UNDEFINED);
        }

        const token = encodeBase64(await services.auth.login(request.body.email, request.body.password));
        response.cookie("token", token, { expires: expirationDate() }).json(new Response({
            token: token
        }));
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /logout requests
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const logout = (services, request, response, next) => {
    try {
        response.clearCookie("token").json(Response.OK);
    } catch (e) {
        next(e);
    }
};

/**
 * Function that defines a callback on /verify requests
 *
 * @param services Services
 * @param request Request
 * @param response Response
 * @param next Next method
 */
const verify = (services, request, response, next) => {
    try {
        response.json(Response.OK);
    } catch (e) {
        next(e);
    }
};

export default {
    "login": new Route(route => route + "/login", "post", login),
    "logout": new AuthRoute(route => route + "/logout", "get", logout),
    "verify": new AuthRoute(route => route + "/verify", "get", verify)
};