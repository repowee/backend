import AuthError from "../common/autherror";
import { AUTH } from "../common/errors";
import { UserRole } from "../model/user";

/**
 * Server route class
 */
class Route {

    /**
     * Constructor
     *
     * @param route Function that build a route with base route in parameter
     * @param method HTTP method
     * @param callback Function callback
     */
    constructor(route, method, callback) {
        this.route = route;
        this.method = method;
        this.callback = callback;
    }

    /**
     * Set route
     *
     * @param route Route
     */
    set route(route) {
        this._route = route;
    }

    /**
     * Get route
     *
     * @return {function} Route
     */
    get route() {
        return this._route;
    }

    /**
     * Set method
     *
     * @param method Method
     */
    set method(method) {
        this._method = method;
    }

    /**
     * Get method
     *
     * @return {string} Method
     */
    get method() {
        return this._method;
    }

    /**
     * Set callback
     *
     * @param callback Callback
     */
    set callback(callback) {
        this._callback = callback;
    }

    /**
     * Get callback
     *
     * @return {function} Callback
     */
    get callback() {
        return this._callback;
    }
}

/**
 * Server route with authentication
 */
class AuthRoute extends Route {

    /**
     * Constructor
     *
     * @param route Function that builds a route with base route in parameter
     * @param method HTTP method
     * @param callback Function callback
     * @param assertUser Predicate to assert logged user
     */
    constructor(route, method, callback, assertUser = () => true) {
        super(route, method, async (services, request, response, next) => {
            // Check cookies
            if (request.cookies === undefined || request.cookies.token === undefined) {
                return next(new AuthError(AUTH.TOKEN_UNDEFINED));
            }

            // Verify token
            try {
                await services.auth.verifyToken(request.cookies.token, assertUser);

                // Forward request
                callback(services, request, response, next);
            } catch (e) {
                next(e);
            }
        });
    }
}

/**
 * Server route with authentication and restricted to administrator
 */
class AdminRoute extends AuthRoute {

    /**
     * Constructor
     *
     * @param route Function that build a route with base route in parameter
     * @param method HTTP method
     * @param callback Function callback
     */
    constructor(route, method, callback) {
        super(route, method, callback, user => user.role === UserRole.ADMIN);
    }
}

const URL_SELF_KEYWORD = "me";

export { Route, AuthRoute, AdminRoute, URL_SELF_KEYWORD };