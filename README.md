# repowee

## Project setup
```
npm install
```

### Run hot-reloads for development
```
npm run start:dev
```

### Run for production
```
npm run build-src
npm run start:prod
```

### Lints and fixes files
```
npm run lint
```
