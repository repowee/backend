import serverConfig from "../../src/config/server";

const SERVER_URL = "http://localhost:" + serverConfig.port;

// Auth
const LOGIN_ROUTE = "/v1/auth/login";
const LOGOUT_ROUTE = "/v1/auth/logout";
const VERIFY_ROUTE = "/v1/auth/verify";

// User
const USER_CREATE_ROUTE = "/v1/user/";
const USER_LIST_ROUTE = "/v1/user/list";
const USER_ROUTE = "/v1/user/";
const USER_SEARCH_ROUTE = "/v1/user/search";
const USER_BATCH_ROUTE = "/v1/user/batch";

// Workspace
const WORKSPACE_ROUTE = (user) => (`/v1/user/${user}/workspace`);

export { SERVER_URL, LOGIN_ROUTE, LOGOUT_ROUTE, VERIFY_ROUTE, USER_CREATE_ROUTE, USER_LIST_ROUTE, USER_ROUTE, USER_SEARCH_ROUTE, USER_BATCH_ROUTE, WORKSPACE_ROUTE };