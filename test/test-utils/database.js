import { config, database } from "../../src/config/database";

/**
 * Init connection with database
 */
const init = async () => {
    // Check connection
    let connected = false;
    try {
        await database.version();
        connected = true;
    } catch (e) {
        // Ignore error
    }

    // Login
    if (!connected) {
        await database.login(config.auth.user, config.auth.password);
    }
};

/**
 * Init connection with database and reset database content
 */
const initAndReset = async () => {
    await init();

    // Reset database
    await reset();
};

/**
 * Reset database (delete all collections)
 */
const reset = async () => {
    // Drop collections
    for (const collection of (await database.listCollections(true))) {
        await database.collection(collection.name).drop();
    }
};

export { init, initAndReset, reset };