import { Workspace } from "../../src/model/workspace";
import { UserCollectionService } from "../../src/services/db/user";

const VALID_WORKSPACE = new Workspace("ckdhvqv", UserCollectionService.COLLECTION_NAME + "/dfyhvbq", "hguvbev");
VALID_WORKSPACE.projects = [];
VALID_WORKSPACE.users = [];

export { VALID_WORKSPACE };