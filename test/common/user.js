import { AuthInfo, User, UserRole } from "../../src/model/user";

const VALID_AUTH = new AuthInfo("kfvbbsvs");
const VALID_USER = new User("Mr test", "test@repowee.fr", "dvbskjvfbsd", UserRole.USER, VALID_AUTH, 10);

export { VALID_AUTH, VALID_USER };