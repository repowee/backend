import { ArangoError } from "arangojs/lib/async/error";
import { assert } from "chai";
import { afterEach, before, beforeEach, describe, it } from "mocha";
import ValidationError from "../../../src/model/error";
import { User, UserRole } from "../../../src/model/user";
import { create, UserCollectionService } from "../../../src/services/db/user";
import { ArangoErrorCode, collectionExists } from "../../../src/utils/database";
import { VALID_USER as REFERENCE_USER } from "../../common/user";
import { initAndReset, reset } from "../../test-utils/database";

let VALID_USER;

describe("UserCollectionService", () => {
    before((done) => {
        initAndReset().then(() => done()).catch(done);
    });

    beforeEach(() => {
        VALID_USER = User.FromObject(REFERENCE_USER.toJSON());

        assert.isTrue(VALID_USER.key === undefined);
    });

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("#create", () => {
        it("Should create a " + UserCollectionService.COLLECTION_NAME + " collection", done => {
            create()
                .then(() => collectionExists(UserCollectionService.COLLECTION_NAME))
                .then(exists => assert.isTrue(exists))
                .then(() => done())
                .catch(done);
        });

        it("Should create an admin", done => {
            create()
                .then(service => service.all())
                .then(users => {
                    assert.lengthOf(users, 1);
                    assert.equal(users[0].mail, UserCollectionService.ADMIN_MAIL);

                    done();
                })
                .catch(done);
        });

        it("Should create an index", done => {
            create()
                .then(service => service.service.indexBy(UserCollectionService.COLLECTION_NAME, index => index.name === UserCollectionService.MAIL_INDEX_NAME))
                .then(index => {
                    assert.isTrue(index !== undefined);

                    done();
                })
                .catch(done);
        });
    });

    describe("#save", () => {
        it("Should throw a duplication error", done => {
            create()
                .then(async service => {
                    await service.save(VALID_USER);
                    return service;
                })
                .then(service => service.save(VALID_USER))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ArangoError);
                    assert.equal(err.errorNum, ArangoErrorCode.UNIQUE_CONSTRAINT_VIOLATED);

                    done();
                });
        });

        it("Should throw a validation error", done => {
            create()
                .then(async service => await service.save(new User(undefined)))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ValidationError);

                    done();
                });
        });

        it("Should save an user", done => {
            create()
                .then(service => service.save(VALID_USER))
                .then(() => {
                    assert.isTrue(VALID_USER.key !== undefined);

                    done();
                })
                .catch(done);
        });

        it("Should save users", done => {
            const VALID_USER_2 = new User(VALID_USER.name, "test2@repowee.fr", "vvbjsnbwbndsbsd", UserRole.USER);

            create()
                .then(service => service.save(VALID_USER, VALID_USER_2))
                .then(() => done())
                .catch(done);
        });
    });

    describe("#update", () => {
        let service;

        it("Should throw a document not found", done => {
            create()
                .then(service => service.update("not_a_key", VALID_USER))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ArangoError);
                    assert.equal(err.errorNum, ArangoErrorCode.DOCUMENT_NOT_FOUND);

                    done();
                });
        });

        it("Should throw a validation error", done => {
            create()
                .then(service => service.update("not_a_key", new User({})))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ValidationError);

                    done();
                });
        });

        it("Should update an object with a partial object", done => {
            const name = "NEW_NAME";

            assert.isFalse(VALID_USER.name === name);

            create()
                .then(async s => {
                    service = s;

                    await s.save(VALID_USER);
                })
                .then(() => service.update(VALID_USER.key, new User(name)))
                .then(() => service.findByKey(VALID_USER.key))
                .then(user => {
                    assert.equal(user.name, name);

                    done();
                })
                .catch(done);
        });

        it("Should update an object with an whole object", done => {
            const name = "NEW_NAME";
            const mail = "newmail@repowee.fr";

            assert.isFalse(VALID_USER.name === name);
            assert.isFalse(VALID_USER.mail === mail);

            create()
                .then(async s => {
                    service = s;

                    await s.save(VALID_USER);
                })
                .then(() => {
                    VALID_USER.name = name;
                    VALID_USER.mail = mail;

                    return service.update(VALID_USER.key, VALID_USER);
                })
                .then(() => service.findByKey(VALID_USER.key))
                .then(user => {
                    assert.equal(user.name, name);
                    assert.equal(user.mail, mail);

                    done();
                })
                .catch(done);
        });
    });

    describe("#findByMail", () => {
        let service;

        it("Should find user", done => {
            create()
                .then(s => {
                    service = s;

                    return s.save(VALID_USER);
                })
                .then(() => service.findByMail(VALID_USER.mail))
                .then(user => {
                    assert.deepEqual(user, VALID_USER);

                    done();
                })
                .catch(done);
        });

        it("Should find nothing", done => {
            create()
                .then(service => service.findByMail("notamail"))
                .then(user => {
                    assert.isTrue(user === undefined);

                    done();
                })
                .catch(done);
        });
    });

    describe("#findByKey", () => {
        it("Should throw an error", done => {
            create()
                .then(service => service.findByKey("not_a_key"))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ArangoError);
                    assert.equal(err.errorNum, ArangoErrorCode.DOCUMENT_NOT_FOUND);

                    done();
                });
        });

        it("Should find an user", done => {
            let service;

            create()
                .then(s => {
                    service = s;

                    return service.all();
                })
                .then(users => service.findByKey(users[0].key))
                .then(() => done())
                .catch(done);
        });
    });
});