import { ArangoError } from "arangojs/lib/async/error";
import { assert } from "chai";
import { afterEach, before, beforeEach, describe, it } from "mocha";
import ValidationError from "../../../src/model/error";
import { User } from "../../../src/model/user";
import { Workspace } from "../../../src/model/workspace";
import { UserCollectionService } from "../../../src/services/db/user";
import { create, WorkspaceCollectionService } from "../../../src/services/db/workspace";
import { ArangoErrorCode, collectionExists } from "../../../src/utils/database";
import { VALID_USER as REFERENCE_USER } from "../../common/user";
import { VALID_WORKSPACE } from "../../common/workspace";
import { initAndReset, reset } from "../../test-utils/database";

let VALID_USER;

describe("WorkspaceCollectionService", () => {
    before((done) => {
        initAndReset().then(() => done()).catch(done);
    });

    beforeEach(() => {
        VALID_USER = User.FromObject(REFERENCE_USER.toJSON());

        assert.isTrue(VALID_USER.key === undefined);
    });

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("#create", () => {
        it("Should create a " + WorkspaceCollectionService.COLLECTION_NAME + " collection", done => {
            create()
                .then(() => collectionExists(WorkspaceCollectionService.COLLECTION_NAME))
                .then(exists => assert.isTrue(exists))
                .then(() => done())
                .catch(done);
        });

        it("Should create an index", done => {
            create()
                .then(service => service.service.indexBy(WorkspaceCollectionService.COLLECTION_NAME, index => index.name === WorkspaceCollectionService.INDEX_NAME))
                .then(index => {
                    assert.isTrue(index !== undefined);

                    done();
                })
                .catch(done);
        });
    });

    describe("#save", () => {
        it("Should throw a duplication error", done => {
            create()
                .then(async service => {
                    await service.save(VALID_WORKSPACE);
                    return service;
                })
                .then(service => service.save(VALID_WORKSPACE))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ArangoError);
                    assert.equal(err.errorNum, ArangoErrorCode.UNIQUE_CONSTRAINT_VIOLATED);

                    done();
                });
        });

        it("Should throw a validation error", done => {
            create()
                .then(service => service.save(new Workspace("vb vbqv", 10, undefined)))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ValidationError);

                    done();
                });
        });

        it("Should save workspace", done => {
            create()
                .then(async service => {
                    await service.save(VALID_WORKSPACE);
                    return service;
                })
                .then(service => service.all())
                .then(workspaces => {
                    assert.lengthOf(workspaces, 1);
                    assert.equal(workspaces[0].name, VALID_WORKSPACE.name);
                    assert.equal(workspaces[0].owner, VALID_WORKSPACE.owner);

                    done();
                })
                .catch(done);
        });

        it("Should save workspaces", done => {
            create()
                .then(async service => {
                    await service.save(VALID_WORKSPACE, new Workspace("edkhvbs", UserCollectionService.COLLECTION_NAME + "/fbsvbsv", "ndfvdsqvq"));
                    return service;
                })
                .then(service => service.all())
                .then(workspaces => {
                    assert.lengthOf(workspaces, 2);

                    done();
                })
                .catch(done);
        });
    });

    describe("#update", () => {
        it("Should throw a document not found", done => {
            create()
                .then(service => service.update("not_a_key", VALID_WORKSPACE))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ArangoError);
                    assert.equal(err.errorNum, ArangoErrorCode.DOCUMENT_NOT_FOUND);

                    done();
                });
        });

        it("Should throw a validation error", done => {
            create()
                .then(service => service.update("not_a_key", new Workspace({})))
                .then(done)
                .catch(err => {
                    assert.instanceOf(err, ValidationError);

                    done();
                });
        });
    });

    describe("#findByOwner", () => {
        it("Should find workspaces", done => {
            const user = new User();
            user.key = VALID_WORKSPACE.owner.split("/")[1];

            create()
                .then(async service => {
                    await service.save(VALID_WORKSPACE);
                    return service;
                })
                .then(service => service.findByOwner(user))
                .then(workspaces => {
                    assert.lengthOf(workspaces, 1);
                    assert.deepEqual(workspaces[0], VALID_WORKSPACE);

                    done();
                })
                .catch(done);
        });

        it("Should find no workspaces", done => {
            create()
                .then(service => service.findByOwner(new User()))
                .then(workspaces => {
                    assert.isEmpty(workspaces);

                    done();
                })
                .catch(done);
        });
    });
});