import { ArangoError } from "arangojs";
import { assert } from "chai";
import { after, afterEach, before, describe, it } from "mocha";
import { database } from "../../../src/config/database";
import { create, UnknownCollection } from "../../../src/services/db/service";
import { ArangoErrorCode, collectionExists } from "../../../src/utils/database";
import { initAndReset, reset } from "../../test-utils/database";

const COLLECTION_NAME = "database-service-spec";

describe("Database service", () => {
    before((done) => {
        initAndReset().then(() => done()).catch(done);
    });

    after(() => database.close());

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("#create", () => {
        it("Should create a service without collections", done => {
            create()
                .then(service => {
                    assert.isEmpty(Object.keys(service.collections));
                    done();
                }).catch(done);
        });

        it("Should create collections given in parameters", done => {
            const SERVICE_COLLECTIONS = [ 1, 2, 3 ].map(i => COLLECTION_NAME + "-" + i);

            create(...SERVICE_COLLECTIONS)
                .then(async service => {
                    assert.lengthOf(Object.keys(service.collections), SERVICE_COLLECTIONS.length);

                    for (const col in service.collections) {
                        assert.isTrue(await collectionExists(col));
                    }
                }).then(() => done()).catch(done);
        });
    });

    describe("#collection", () => {
        it("Should get collection", done => {
            create(COLLECTION_NAME)
                .then(service => service.collection(COLLECTION_NAME).drop())
                .then(() => done()).catch(done);
        });

        it("Should throw an error", done => {
            create(COLLECTION_NAME)
                .then(service => service.collection("NotACollection"))
                .then(() => done("Should throw an error"))
                .catch(e => {
                    assert.instanceOf(e, UnknownCollection);

                    done();
                });
        });
    });

    describe("#findByKey", () => {
        it("Should find a document", done => {
            const OBJ = {
                test: "a",
                value: "b"
            };

            let service;
            create(COLLECTION_NAME)
                .then(s => {
                    service = s;
                    return s.collection(COLLECTION_NAME).save(OBJ);
                })
                .then(info => service.findByKey(COLLECTION_NAME, info._key))
                .then(doc => {
                    // Delete
                    delete doc._key;
                    delete doc._id;
                    delete doc._rev;

                    assert.deepEqual(OBJ, doc);
                    done();
                }).catch(done);
        });

        it("Should throw an error", done => {
            create(COLLECTION_NAME)
                .then(service => service.findByKey(COLLECTION_NAME, "not_a_key"))
                .then(() => done("Should throw an error"))
                .catch(e => {
                    assert.instanceOf(e, ArangoError);
                    assert.equal(e.errorNum, ArangoErrorCode.DOCUMENT_NOT_FOUND);

                    done();
                });
        });
    });

    describe("#findBy", () => {
        it("Should find documents", done => {
            const OBJECTS_COUNT = 10;

            let service;
            create(COLLECTION_NAME)
                .then(async s => {
                    service = s;

                    for (let i = 0; i < OBJECTS_COUNT; i++) {
                        const obj = {
                            index: i
                        };

                        if (obj.index % 2 === 0) {
                            obj.data = {
                                p: i * i
                            };
                        }

                        await s.collection(COLLECTION_NAME).save(obj);
                    }
                })
                .then(() => service.findBy(COLLECTION_NAME, "index", 6))
                .then(docs => {
                    assert.lengthOf(docs, 1);
                    assert.equal(docs[0].index, 6);

                    return service.findBy(COLLECTION_NAME, "data.p", 16);
                })
                .then(docs => {
                    assert.lengthOf(docs, 1);
                    assert.equal(docs[0].index, 4);

                    return service.findBy(COLLECTION_NAME, "index", 6, false);
                })
                .then(docs => {
                    assert.lengthOf(docs, OBJECTS_COUNT - 1);

                    done();
                }).catch(done);
        });

        it("Should find no documents", done => {
            create(COLLECTION_NAME)
                .then(service => service.findBy(COLLECTION_NAME, "test.value.version.isAvailable", true))
                .then(docs => {
                    assert.isEmpty(docs);

                    done();
                }).catch(done);
        });
    });

    describe("#all", () => {
        it("Should retrieve all documents", done => {
            const OBJECTS_COUNT = 10;

            create(COLLECTION_NAME)
                .then(async service => {
                    for (let i = 0; i < OBJECTS_COUNT; i++) {
                        await service.collection(COLLECTION_NAME).save({
                            index: i
                        });
                    }
                    return service;
                })
                .then(service => service.all(COLLECTION_NAME))
                .then(docs => {
                    assert.lengthOf(docs, OBJECTS_COUNT);

                    done();
                }).catch(done);
        });

        it("Should retrieve no documents", done => {
            create(COLLECTION_NAME)
                .then(async service => {
                    assert.isEmpty(await service.all(COLLECTION_NAME));
                })
                .then(() => done()).catch(done);
        });
    });

    describe("#replace", () => {
        it("Should replace the document", done => {
            const obj = {
                key: "a"
            };

            let service;
            create(COLLECTION_NAME)
                .then(async s => {
                    service = s;
                    return database.collection(COLLECTION_NAME).save(obj);
                })
                .then(info => {
                    return service.replace(COLLECTION_NAME, info._key, {
                        value: "b"
                    });
                })
                .then(async () => {
                    const docs = await service.all(COLLECTION_NAME);

                    assert.lengthOf(docs, 1);
                    assert.equal(docs[0].value, "b");
                })
                .then(() => done()).catch(done);
        });

        it("Should throw an error", done => {
            create(COLLECTION_NAME)
                .then(service => service.replace(COLLECTION_NAME, "not_a_key", {}))
                .then(() => done("Should throw an error"))
                .catch(e => {
                    assert.instanceOf(e, ArangoError);
                    assert.equal(e.errorNum, ArangoErrorCode.DOCUMENT_NOT_FOUND);

                    done();
                });
        });
    });

    describe("#update", () => {
        it("Should update the document", done => {
            const old = {
                key: "a"
            };
            const _new = {
                value: "b"
            };

            let service;
            create(COLLECTION_NAME)
                .then(s => {
                    service = s;

                    return database.collection(COLLECTION_NAME).save(old);
                })
                .then(info => {
                    return service.update(COLLECTION_NAME, info._key, _new);
                })
                .then(info => {
                    // Delete fields
                    delete info.new._key;
                    delete info.new._id;
                    delete info.new._rev;
                    delete info.old._key;
                    delete info.old._id;
                    delete info.old._rev;

                    assert.deepEqual(info.old, old);
                    assert.deepEqual(info.new, {
                        key: "a",
                        value: "b"
                    });

                    done();
                }).catch(done);
        });

        it("Should throw an error", done => {
            create(COLLECTION_NAME)
                .then(service => service.update(COLLECTION_NAME, "not_a_key", {}))
                .then(() => done("Should throw an error"))
                .catch(e => {
                    assert.instanceOf(e, ArangoError);
                    assert.equal(e.errorNum, ArangoErrorCode.DOCUMENT_NOT_FOUND);

                    done();
                });
        });
    });

    describe("#remove", () => {
        it("Should remove document", done => {
            let service;
            create(COLLECTION_NAME)
                .then(s => {
                    service = s;

                    return database.collection(COLLECTION_NAME).save({
                        test: true
                    });
                })
                .then(async info => {
                    const docs = await service.all(COLLECTION_NAME);
                    assert.lengthOf(docs, 1);

                    await service.remove(COLLECTION_NAME, info._key);
                })
                .then(async () => {
                    const docs = await service.all(COLLECTION_NAME);

                    assert.isEmpty(docs);

                    done();
                }).catch(done);
        });

        it("Should throw an error", done => {
            create(COLLECTION_NAME)
                .then(service => service.remove(COLLECTION_NAME, "not_a_key"))
                .then(() => done("Should throw an error"))
                .catch(e => {
                    assert.instanceOf(e, ArangoError);
                    assert.equal(e.errorNum, ArangoErrorCode.DOCUMENT_NOT_FOUND);

                    done();
                });
        });
    });

    describe("#exists", () => {
        it("Should find a document", done => {
            let service;
            create(COLLECTION_NAME)
                .then(s => {
                    service = s;

                    return database.collection(COLLECTION_NAME).save({
                        test: true
                    });
                })
                .then(async info => {
                    assert.isTrue(await service.exists(COLLECTION_NAME, info._key));
                })
                .then(() => done()).catch(done);
        });

        it("Should find no document", done => {
            create(COLLECTION_NAME)
                .then(async service => {
                    assert.isFalse(await service.exists(COLLECTION_NAME, "not_a_key"));
                })
                .then(() => done()).catch(done);
        });
    });

    describe("#save", () => {
        it("Should save nothing", done => {
            create(COLLECTION_NAME)
                .then(async service => {
                    const info = await service.save(COLLECTION_NAME);
                    assert.isEmpty(info);

                    return service;
                })
                .then(async service => {
                    assert.isEmpty(await service.all(COLLECTION_NAME));

                    done();
                }).catch(done);
        });

        it("Should save an object", done => {
            create(COLLECTION_NAME)
                .then(async service => {
                    const info = await service.save(COLLECTION_NAME, {
                        test: true
                    });
                    assert.lengthOf(info, 1);

                    return service;
                })
                .then(async service => {
                    assert.lengthOf(await service.all(COLLECTION_NAME), 1);

                    done();
                }).catch(done);
        });

        it("Should save objects", done => {
            const OBJS = [ 1, 2, 3 ].map(i => ({
                index: i
            }));

            create(COLLECTION_NAME)
                .then(async service => {
                    const info = await service.save(COLLECTION_NAME, ...OBJS);
                    assert.lengthOf(info, OBJS.length);

                    return service;
                })
                .then(async service => {
                    assert.lengthOf(await service.all(COLLECTION_NAME), OBJS.length);

                    done();
                }).catch(done);
        });
    });

    describe("#indexByName", () => {
        it("Should find the index", done => {
            const INDEX_NAME = "index-name";
            let service;

            create(COLLECTION_NAME)
                .then(s => {
                    service = s;

                    return database.collection(COLLECTION_NAME).createIndex({
                        name: INDEX_NAME,
                        fields: [ "name", "key", "value" ],
                        unique: true,
                        type: "hash"
                    });
                })
                .then(() => {
                    return service.indexBy(COLLECTION_NAME, index => index.name === INDEX_NAME);
                })
                .then(index => {
                    assert.equal(index.name, INDEX_NAME);

                    done();
                })
                .catch(done);
        });

        it("Should find no index", done => {
            create(COLLECTION_NAME)
                .then(service => service.indexBy(COLLECTION_NAME, index => index.name === "NotAndIndex"))
                .then(index => {
                    assert.equal(index, undefined);

                    done();
                })
                .catch(done);
        });
    });

    describe("#createIndex", () => {
        it("Should create an index", done => {
            create(COLLECTION_NAME)
                .then(service => service.createIndex(COLLECTION_NAME, {
                    name: "index",
                    fields: [ "name", "key", "value" ],
                    unique: true,
                    type: "hash"
                }))
                .then(() => done())
                .catch(done);
        });
    });

    describe("#executeTransaction", () => {
        it("Should abort the transaction", done => {
            let service;

            create(COLLECTION_NAME)
                .then(async s => {
                    service = s;

                    await service.executeTransaction([
                        () => service.save(COLLECTION_NAME, { test: "test" }),
                        () => {
                            throw Error("Abort transaction");
                        }
                    ], {
                        "read": COLLECTION_NAME,
                        "write": COLLECTION_NAME
                    });
                })
                .then(done)
                .catch(async () => {
                    assert.isEmpty(await service.all(COLLECTION_NAME));

                    done();
                });
        });

        it("Should execute action", done => {
            const object = { test: "test" };

            create(COLLECTION_NAME)
                .then(async service => {
                    await service.executeTransaction([
                        () => service.save(COLLECTION_NAME, object)
                    ], {
                        "read": COLLECTION_NAME,
                        "write": COLLECTION_NAME
                    });

                    return service;
                })
                .then(service => service.all(COLLECTION_NAME))
                .then(documents => {
                    delete documents[0]._id;
                    delete documents[0]._key;
                    delete documents[0]._rev;

                    assert.deepEqual(documents[0], object);
                    done();
                })
                .catch(done);
        });

        it("Should execute actions synchronously", done => {
            const object = { test: "test" };

            create(COLLECTION_NAME)
                .then(async service => {
                    await service.executeTransaction([
                        () => service.save(COLLECTION_NAME, object),
                        results => service.save(COLLECTION_NAME, { test: results[0][0]._id })
                    ], {
                        "read": COLLECTION_NAME,
                        "write": COLLECTION_NAME
                    });

                    return service;
                })
                .then(service => service.all(COLLECTION_NAME))
                .then(documents => {
                    assert.lengthOf(documents, 2);
                    assert.equal(documents[1].test, documents[0]._id);
                    done();
                })
                .catch(done);
        });
    });
});