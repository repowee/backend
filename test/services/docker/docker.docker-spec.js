import { assert } from "chai";
import { describe, it } from "mocha";
import { DockerService } from "../../../src/services/docker/docker";
import { readdirSync, createWriteStream } from "fs";

const goodProjetFolderPath = "./test/services/docker/goodProjects/";
const badProjectsFolderPath = "./test/services/docker/badBuildParameters/";

const docker = new DockerService();

/**
 * Docker test
 *
 * test that build with good config file and bad build parameters fail
 * test that run with good config, successffull build and bad run parameters fail
 */

describe("Docker service", () => {

    describe("Build Images with wrong build parameters", () => {
        let badProjects = readdirSync(badProjectsFolderPath);

        badProjects.forEach(badProject => {
            // config file must be ok
            let config = DockerService.checkConfigFile(badProjectsFolderPath + badProject + "/");

            it(badProject + " build should fail", (done) => {
                docker.createDockerfile(badProject, config);
                docker.buildDockerImage(badProject, config)
                    .then(() => done("Should throw an error"))
                    .catch(e => {
                        assert.instanceOf(e, Error);
                        done();
                    });
            });
        });
    });
    describe("Run Containers", () => {
        let projects = readdirSync(goodProjetFolderPath);

        projects.forEach(project => {
            it(project + " build must be ok", (done) => {
                let containerOutput = createWriteStream("./tmp/tmp-" + project + "-ContainersOutput.txt");

                docker.buildImageFromFolder(goodProjetFolderPath + project + "/").then((imageName) => {
                    assert.isNotNull(imageName);
                    assert.typeOf(imageName, "String");

                    docker.runContainer(imageName, containerOutput).then((containerId) => {
                        assert.isNotNull(containerId);
                        assert.typeOf(containerId, "string");

                        // The container is runing, stopping the container
                        docker.stopContainer(containerId)
                            .then(() => {
                                done();
                            })
                            .catch(() => {
                                done("Stoping the container shouldn't throw an error");
                            });

                    }).catch(() => {
                        done("The run of the image shouldn't throw an error");
                    });

                }).catch(() => {
                    done("The Build of the project shouldn't throw an error");
                });
            });
        });
    });
    describe("Run Containers with wrong parameters", () => {
        it("Run a container with a wrong stream sould fail", (done) => {
            let projectPath = goodProjetFolderPath + "goodproject_simpleUsageCase/";

            docker.buildImageFromFolder(projectPath).then((imageName) => {
                assert.isNotNull(imageName);
                assert.typeOf(imageName, "String");

                try {
                    docker.runContainer(imageName, "IAMNOTASTREAM").then(() => {
                        done("The run should have failed");
                    });
                } catch (e) {
                    done();
                }

            }).catch(() => {
                done("The Build of the project shouldn't throw an error");
            });

        });
    });
    describe("other functions", () => {
        it("getAllmages must return a list of images", (done) => {
            docker.getAllImages().then(images => {
                assert.isNotNull(images);
                assert.typeOf(images, "Array");
                assert.isNotEmpty(images);
                done();
            });
        });

        it("getAllContainers must return a list of containers", (done) => {
            docker.getAllContainers().then(containers => {
                assert.isNotNull(containers);
                assert.typeOf(containers, "Array");
                done();
            });
        });

        it("stopAllContainers must stop all containers", (done) => {
            let projectPath = goodProjetFolderPath + "goodproject_simpleUsageCase/";

            docker.buildImageFromFolder(projectPath).then((imageName) => {
                assert.isNotNull(imageName);
                assert.typeOf(imageName, "String");

                docker.runContainer(imageName, null).then((containerId) => {
                    assert.isNotNull(containerId);
                    assert.typeOf(containerId, "string");

                    // The container is runing, check existing
                    docker.getAllContainers().then(containers => {
                        assert.isNotNull(containers);
                        assert.typeOf(containers, "Array");
                        assert.isNotEmpty(containers);

                        docker.stopAllContainers()
                            .then(() => {
                                docker.getAllContainers().then(containers => {
                                    assert.isNotNull(containers);
                                    assert.typeOf(containers, "Array");
                                    assert.isEmpty(containers);
                                    done();
                                });
                            })
                            .catch(() => {
                                done("Stoping all containers shouldn't throw an error");
                            });
                    });

                }).catch(() => {
                    done("The run of the image shouldn't throw an error");
                });

            }).catch(() => {
                done("The Build of the project shouldn't throw an error");
            });
        });

        it("removeAllImage must remove all images", (done) => {
            docker.removeAllImages().then(() => {
                docker.getAllImages().then(images => {
                    assert.isNotNull(images);
                    assert.typeOf(images, "Array");
                    assert.isEmpty(images);
                    done();
                });
            });
        });
    });
});