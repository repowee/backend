import { assert } from "chai";
import { describe, it } from "mocha";
import { DockerService } from "../../../src/services/docker/docker";
import { readdirSync } from "fs";

/**
 * Docker config file test
 *
 * test that bad config file fail but that the good one succed
 */

describe("Docker service checkConfigFile", () => {
    describe("CheckConfigFile with bad config file", () => {

        // Unexistant project :
        let unexistantProjectPath = "./test/services/docker/IDONTEXIST/";

        it("unexistantProject check config file must throw an error", () => {
            assert.throws(() => DockerService.checkConfigFile(unexistantProjectPath), Error);
        });

        // Get all bad projects
        let badProjectsFolderPath = "./test/services/docker/badConfigFile/";
        let badProjects = readdirSync(badProjectsFolderPath);

        badProjects.forEach(badProject => {
            it(badProject + " check config file must throw an error", () => {
                assert.throws(() => DockerService.checkConfigFile(badProjectsFolderPath + badProject + "/"), Error);
            });
        });
    });
    describe("CheckConfigFile with good config file", () => {
        // Get all good projects
        let badBuildFolderPath = "./test/services/docker/badBuildParameters/";
        let badBuildProjects = readdirSync(badBuildFolderPath);

        let goodProjetFolderPath = "./test/services/docker/goodProjects/";
        let goodProjects = readdirSync(goodProjetFolderPath);

        badBuildProjects.forEach(badBuildProject => {
            it(badBuildProject + " config file must be ok", () => {
                let config = DockerService.checkConfigFile(badBuildFolderPath + badBuildProject + "/");
                assert.isNotNull(config);
                assert.typeOf(config, "Object");
            });
        });

        goodProjects.forEach(goodProject => {
            it(goodProject + " config file must be ok", () => {
                let config = DockerService.checkConfigFile(goodProjetFolderPath + goodProject + "/");
                assert.isNotNull(config);
                assert.typeOf(config, "Object");
            });
        });
    });
});