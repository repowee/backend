import chai from "chai";
import fs from "fs";
import { after, afterEach, before, beforeEach, describe, it } from "mocha";
import { AUTH, HTTP, USER_API } from "../../src/common/errors";
import { application as appConfig, server as serverConfig } from "../../src/config/config";
import { database } from "../../src/config/database";
import { UserRole } from "../../src/model/user";
import { URL_SELF_KEYWORD } from "../../src/router/route";
import { create as createUserDBService, UserCollectionService } from "../../src/services/db/user";
import { create as createWorkspaceDBService } from "../../src/services/db/workspace";
import { UserService } from "../../src/services/user";
import { hash } from "../../src/utils/hash";
import { initAndReset, reset } from "../test-utils/database";
import { parseJSONResponse } from "../test-utils/http";
import { LOGIN_ROUTE, SERVER_URL, USER_BATCH_ROUTE, USER_CREATE_ROUTE, USER_LIST_ROUTE, USER_ROUTE, USER_SEARCH_ROUTE } from "../test-utils/server";

// Add HTTP
chai.use(require("chai-http"));

const USER_MAIL = "test@test.fr";
const USER_PASSWORD = "test";

let admin_token;
let service;

describe("UserAPI", () => {
    before(done => {
        initAndReset()
            .then(() => done())
            .catch(done);
    });

    beforeEach(done => {
        createUserDBService()
            .then(s => {
                service = s;
                return chai.request(SERVER_URL)
                    .post(LOGIN_ROUTE)
                    .send({
                        email: UserCollectionService.ADMIN_MAIL,
                        password: "beatsaber"
                    });
            })
            .then(response => {
                chai.assert.equal(response.status, 200);
                admin_token = parseJSONResponse(response).data.token;

                return createWorkspaceDBService();
            })
            .then(() => done())
            .catch(done);
    });

    after(() => {
        // Close database
        database.close();
    });

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("create", () => {
        it("Should set user's name", done => {
            chai.request(SERVER_URL)
                .post(USER_CREATE_ROUTE)
                .set("cookie", "token=" + admin_token)
                .send({
                    mail: USER_MAIL,
                    password: USER_PASSWORD,
                    role: UserRole.USER
                })
                .then(async response => {
                    chai.assert.equal(response.status, 200);

                    const user = await service.findByMail(USER_MAIL);
                    chai.assert.equal(user.name, user.mail.split("@")[0]);

                    done();
                })
                .catch(done);
        });

        it("Should hash password", done => {
            chai.request(SERVER_URL)
                .post(USER_CREATE_ROUTE)
                .set("cookie", "token=" + admin_token)
                .send({
                    mail: USER_MAIL,
                    password: USER_PASSWORD,
                    role: UserRole.USER
                })
                .then(() => service.findByMail(USER_MAIL))
                .then(async user => {
                    chai.assert.notEqual(user, undefined);
                    chai.assert.equal(user.password, await hash(USER_PASSWORD, serverConfig.auth.salt));

                    done();
                })
                .catch(done);
        });

        it("Should save password", done => {
            let password;
            hash(USER_PASSWORD, serverConfig.auth.salt)
                .then(p => {
                    password = p;

                    return chai.request(SERVER_URL)
                        .post(USER_CREATE_ROUTE)
                        .set("cookie", "token=" + admin_token)
                        .send({
                            mail: USER_MAIL,
                            password: password,
                            role: UserRole.USER
                        });
                })
                .then(() => service.findByMail(USER_MAIL))
                .then(user => {
                    chai.assert.notEqual(user, undefined);
                    chai.assert.equal(user.password, password);

                    done();
                })
                .catch(done);
        });

        it("Should discard auth field", done => {
            chai.request(SERVER_URL)
                .post(USER_CREATE_ROUTE)
                .set("cookie", "token=" + admin_token)
                .send({
                    mail: USER_MAIL,
                    password: USER_PASSWORD,
                    role: UserRole.USER,
                    auth: {
                        token: "dvbvfybjqhsdvqehbc"
                    }
                })
                .then(() => service.findByMail(USER_MAIL))
                .then(async user => {
                    chai.assert.notEqual(user, undefined);
                    chai.assert.equal(user.auth, undefined);

                    done();
                })
                .catch(done);
        });

        it("Should reject user", done => {
            chai.request(SERVER_URL)
                .post(USER_CREATE_ROUTE)
                .set("cookie", "token=" + admin_token)
                .send({
                    mail: USER_MAIL,
                    password: USER_PASSWORD,
                    role: UserRole.USER
                })
                .then(() => chai.request(SERVER_URL)
                    .post(LOGIN_ROUTE)
                    .send({
                        email: USER_MAIL,
                        password: USER_PASSWORD
                    }))
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .post(USER_CREATE_ROUTE)
                        .set("cookie", "token=" + parseJSONResponse(response).data.token)
                        .send({
                            mail: "test2@test.fr",
                            password: USER_PASSWORD,
                            role: UserRole.USER
                        });
                })
                .then(response => {
                    chai.assert.equal(response.status, 403);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.UNAUTHORIZED_USER);

                    done();
                })
                .catch(done);
        });
    });

    describe("list", () => {
        it("Should return all users", done => {
            chai.request(SERVER_URL)
                .get(USER_LIST_ROUTE)
                .set("cookie", "token=" + admin_token)
                .then(async response => {
                    chai.assert.equal(response.status, 200);
                    chai.assert.sameDeepMembers(parseJSONResponse(response).data, (await service.all()).map(UserService.ToAPIObject));

                    done();
                })
                .catch(done);
        });
    });

    describe("search", () => {
        it("Should find a user", done => {
            chai.request(SERVER_URL)
                .get(USER_SEARCH_ROUTE)
                .query({ query: UserCollectionService.ADMIN_MAIL.split("@")[0] })
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 200);
                    chai.assert.isNotEmpty(parseJSONResponse(response).data.filter(u => u.mail === UserCollectionService.ADMIN_MAIL));

                    done();
                })
                .catch(done);
        });

        it("Should find nothing", done => {
            chai.request(SERVER_URL)
                .get(USER_SEARCH_ROUTE)
                .query({ query: "notamail" })
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 200);
                    chai.assert.isEmpty(parseJSONResponse(response).data);

                    done();
                })
                .catch(done);
        });

        it("Should throw an error", done => {
            chai.request(SERVER_URL)
                .get(USER_SEARCH_ROUTE)
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.notEqual(response.status, 200);
                    chai.assert.equal(parseJSONResponse(response).error, USER_API.QUERY_UNDEFINED);

                    done();
                })
                .catch(done);
        });
    });

    describe("get", () => {
        it("Should get an user", done => {
            chai.request(SERVER_URL)
                .get(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    const user = parseJSONResponse(response).data;
                    chai.assert.equal(user.role, UserRole.ADMIN);
                    chai.assert.equal(user.mail, UserCollectionService.ADMIN_MAIL);

                    done();
                })
                .catch(done);
        });

        it("Should get current user", done => {
            chai.request(SERVER_URL)
                .get(USER_ROUTE + URL_SELF_KEYWORD)
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    const user = parseJSONResponse(response).data;
                    chai.assert.equal(user.role, UserRole.ADMIN);
                    chai.assert.equal(user.mail, UserCollectionService.ADMIN_MAIL);

                    done();
                })
                .catch(done);
        });

        it("Should get nothing", done => {
            chai.request(SERVER_URL)
                .get(USER_ROUTE + "notamail")
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 404);
                    chai.assert.equal(parseJSONResponse(response).error, USER_API.USER_NOT_FOUND);

                    done();
                })
                .catch(done);
        });
    });

    describe("update", () => {
        const NEW_ADMIN_NAME = "New Administrator";
        const NEW_WORKSPACE_LIMIT = 5;
        const NEW_MAIL = "newadmin@repowee.fr";

        it("Should update user", done => {
            chai.request(SERVER_URL)
                .patch(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                .set("cookie", "token=" + admin_token)
                .send({ name: NEW_ADMIN_NAME, workspacesLimit: NEW_WORKSPACE_LIMIT })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .get(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                        .set("cookie", "token=" + admin_token);
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    const user = parseJSONResponse(response).data;
                    chai.assert.equal(user.name, NEW_ADMIN_NAME);
                    chai.assert.equal(user.workspacesLimit, NEW_WORKSPACE_LIMIT);

                    done();
                })
                .catch(done);
        });

        it("Should update current user", done => {
            chai.request(SERVER_URL)
                .patch(USER_ROUTE + "me")
                .set("cookie", "token=" + admin_token)
                .send({ name: NEW_ADMIN_NAME, mail: NEW_MAIL })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .get(USER_ROUTE + "me")
                        .set("cookie", "token=" + admin_token);
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    const user = parseJSONResponse(response).data;
                    chai.assert.equal(user.name, NEW_ADMIN_NAME);
                    chai.assert.equal(user.mail, NEW_MAIL);

                    done();
                })
                .catch(done);
        });

        it("Should update no fields", done => {
            let old_user;

            chai.request(SERVER_URL)
                .get(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 200);
                    old_user = parseJSONResponse(response).data;

                    return chai.request(SERVER_URL)
                        .patch(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                        .set("cookie", "token=" + admin_token)
                        .send({});
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .get(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                        .set("cookie", "token=" + admin_token);
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);
                    chai.assert.deepEqual(parseJSONResponse(response).data, old_user);

                    done();
                })
                .catch(done);
        });

        it("Should update password", done => {
            const NEW_PASSWORD = "newpassword";

            chai.request(SERVER_URL)
                .patch(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                .set("cookie", "token=" + admin_token)
                .send({ password: NEW_PASSWORD })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .post(LOGIN_ROUTE)
                        .send({
                            email: UserCollectionService.ADMIN_MAIL,
                            password: NEW_PASSWORD
                        });
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    done();
                })
                .catch(done);
        });

        it("Should update nobody", done => {
            chai.request(SERVER_URL)
                .patch(USER_ROUTE + "notamail")
                .set("cookie", "token=" + admin_token)
                .send({})
                .then(response => {
                    chai.assert.equal(response.status, 404);
                    chai.assert.equal(parseJSONResponse(response).error, USER_API.USER_NOT_FOUND);

                    done();
                })
                .catch(done);
        });
    });

    describe("delete", () => {
        it("Should delete user", done => {
            chai.request(SERVER_URL)
                .delete(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .get(USER_ROUTE + UserCollectionService.ADMIN_MAIL)
                        .set("cookie", "token=" + admin_token);
                })
                .then(response => {
                    chai.assert.equal(response.status, 403);

                    done();
                })
                .catch(done);
        });

        it("Should delete current user", done => {
            chai.request(SERVER_URL)
                .delete(USER_ROUTE + URL_SELF_KEYWORD)
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .get(USER_ROUTE + URL_SELF_KEYWORD)
                        .set("cookie", "token=" + admin_token);
                })
                .then(response => {
                    chai.assert.equal(response.status, 403);

                    done();
                })
                .catch(done);
        });

        it("Should delete nothing", done => {
            chai.request(SERVER_URL)
                .delete(USER_ROUTE + "notamail")
                .set("cookie", "token=" + admin_token)
                .then(response => {
                    chai.assert.equal(response.status, 404);
                    chai.assert.equal(parseJSONResponse(response).error, USER_API.USER_NOT_FOUND);

                    done();
                })
                .catch(done);
        });
    });

    describe("batch", () => {
        it("Should save users", done => {
            chai.request(SERVER_URL)
                .post(USER_BATCH_ROUTE)
                .set("cookie", "token=" + admin_token)
                .attach("data", fs.readFileSync("./test/integration/files/users.csv"), "users.csv")
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .get(USER_LIST_ROUTE)
                        .set("cookie", "token=" + admin_token);
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    chai.assert.includeDeepMembers(parseJSONResponse(response).data, [
                        { mail: "test@test.fr", role: UserRole.USER, workspacesLimit: appConfig.workspacesLimit },
                        { mail: "test2@test.fr", role: UserRole.USER, workspacesLimit: appConfig.workspacesLimit },
                        { mail: "test3@test.fr", role: UserRole.USER, workspacesLimit: appConfig.workspacesLimit }
                    ]);

                    done();
                })
                .catch(done);
        });

        it("Should throw an DB error", done => {
            chai.request(SERVER_URL)
                .post(USER_BATCH_ROUTE)
                .set("cookie", "token=" + admin_token)
                .attach("data", fs.readFileSync("./test/integration/files/users_with_errors.csv"), "users.csv")
                .then(async response => {
                    chai.assert.notEqual(response.status, 200);

                    const users = await service.all();
                    chai.assert.lengthOf(users, 1);

                    done();
                })
                .catch(done);
        });

        it("Should throw an DATA_UNDEFINED error", done => {
            chai.request(SERVER_URL)
                .post(USER_BATCH_ROUTE)
                .set("cookie", "token=" + admin_token)
                .send()
                .then(response => {
                    chai.assert.notEqual(response.status, 200);
                    chai.assert.equal(parseJSONResponse(response).error, HTTP.DATA_UNDEFINED);

                    done();
                })
                .catch(done);
        });
    });
});