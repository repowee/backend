import { decode as decodeBase64, encode as encodeBase64 } from "base64-url";
import chai from "chai";
import { sign, verify } from "jsonwebtoken";
import { after, afterEach, before, beforeEach, describe, it } from "mocha";
import { AUTH } from "../../src/common/errors";
import { database } from "../../src/config/database";
import serverConfig from "../../src/config/server";
import { User, UserRole } from "../../src/model/user";
import { create } from "../../src/services/db/user";
import { hash } from "../../src/utils/hash";
import { initAndReset, reset } from "../test-utils/database";
import { parseJSONResponse } from "../test-utils/http";
import { LOGIN_ROUTE, LOGOUT_ROUTE, SERVER_URL, VERIFY_ROUTE } from "../test-utils/server";

// Add HTTP
chai.use(require("chai-http"));

const USER_EMAIL = "test@test.fr";
const USER_PASS = "secretpassword";

let user;

describe("AuthenticationAPI", () => {
    before(done => {
        initAndReset().then(async () => {
            user = new User("test-user", USER_EMAIL, await hash(USER_PASS, serverConfig.auth.salt), UserRole.USER);

            done();
        }).catch(done);
    });

    after(() => {
        // Close database
        database.close();
    });

    beforeEach(done => {
        create().then(service => service.save(user)).then(() => done()).catch(done);
    });

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("Login", () => {
        it("No body", done => {
            chai.request(SERVER_URL).post(LOGIN_ROUTE)
                .then(response => {
                    chai.assert.equal(response.status, 500);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.EMAIL_UNDEFINED);

                    done();
                }).catch(done);
        });

        it("Email and no password", done => {
            chai.request(SERVER_URL).post(LOGIN_ROUTE).send({ email: "notaemail" })
                .then(response => {
                    chai.assert.equal(response.status, 500);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.PASSWORD_UNDEFINED);

                    done();
                }).catch(done);
        });

        it("Password and no email", done => {
            chai.request(SERVER_URL).post(LOGIN_ROUTE).send({ password: "password" })
                .then(response => {
                    chai.assert.equal(response.status, 500);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.EMAIL_UNDEFINED);

                    done();
                }).catch(done);
        });

        it("Unknown user", done => {
            chai.request(SERVER_URL).post(LOGIN_ROUTE).send({ email: "unknown", password: "password" })
                .then(response => {
                    chai.assert.equal(response.status, 403);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.INVALID_CREDENTIALS);

                    done();
                }).catch(done);
        });

        it("Invalid password", done => {
            chai.request(SERVER_URL).post(LOGIN_ROUTE).send({ email: USER_EMAIL, password: "notthepassword" })
                .then(response => {
                    chai.assert.equal(response.status, 403);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.INVALID_CREDENTIALS);

                    done();
                }).catch(done);
        });

        it("Valid user", done => {
            let token;

            chai.request(SERVER_URL).post(LOGIN_ROUTE).send({ email: USER_EMAIL, password: USER_PASS })
                .then(response => {
                    token = parseJSONResponse(response).data.token;

                    chai.assert.equal(response.status, 200);
                    chai.assert.include(response.header["set-cookie"][0], "token=" + token);
                    chai.assert.typeOf(token, "string");
                    verify(decodeBase64(token), serverConfig.auth.salt);

                    return create();
                })
                .then(async service => {
                    const user = await service.findByMail(USER_EMAIL);
                    chai.assert.equal(decodeBase64(token), user.auth.token);

                    done();
                }).catch(done);
        });
    });

    describe("Logout", () => {
        it("No token", done => {
            chai.request(SERVER_URL).get(LOGOUT_ROUTE)
                .then(response => {
                    chai.assert.equal(response.status, 403);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.TOKEN_UNDEFINED);

                    done();
                }).catch(done);
        });

        it("Expired token", done => {
            chai.request(SERVER_URL)
                .get(LOGOUT_ROUTE)
                .set("cookie", "token=" + encodeBase64(sign({ data: "random" }, serverConfig.auth.salt, { expiresIn: "-10days" })))
                .then(response => {
                    chai.assert.equal(response.status, 403);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.EXPIRED_TOKEN);

                    done();
                }).catch(done);
        });

        it("Unknown token", done => {
            chai.request(SERVER_URL)
                .get(LOGOUT_ROUTE)
                .set("cookie", "token=" + encodeBase64(sign({ data: "random" }, serverConfig.auth.salt, { expiresIn: serverConfig.auth.tokenDuration })))
                .then(response => {
                    chai.assert.equal(response.status, 403);
                    chai.assert.equal(parseJSONResponse(response).error, AUTH.UNKNOWN_TOKEN);

                    done();
                }).catch(done);
        });

        it("Valid token", done => {
            chai.request(SERVER_URL)
                .post(LOGIN_ROUTE)
                .send({
                    email: USER_EMAIL,
                    password: USER_PASS
                })
                .then(response => chai.request(SERVER_URL).get(LOGOUT_ROUTE).set("cookie", "token=" + parseJSONResponse(response).data.token))
                .then(response => {
                    chai.assert.equal(response.status, 200);
                    chai.assert.equal(response.header["set-cookie"][0].split(";")[0], "token=");

                    done();
                }).catch(done);
        });
    });

    describe("verify", () => {
        it("", done => {
            chai.request(SERVER_URL)
                .post(LOGIN_ROUTE)
                .send({
                    email: USER_EMAIL,
                    password: USER_PASS
                })
                .then(response => chai.request(SERVER_URL).get(VERIFY_ROUTE).set("cookie", "token=" + parseJSONResponse(response).data.token))
                .then(response => {
                    chai.assert.equal(response.status, 200);
                    chai.assert.equal(parseJSONResponse(response).data, "OK");

                    done();
                })
                .catch(done);
        });
    });
});