import chai from "chai";
import { after, afterEach, before, beforeEach, describe, it } from "mocha";
import { WORKSPACE_API } from "../../src/common/errors";
import { database } from "../../src/config/database";
import { User, UserRole } from "../../src/model/user";
import { create as createUserDBService, UserCollectionService } from "../../src/services/db/user";
import { create as createWorkspaceDBService } from "../../src/services/db/workspace";
import { initAndReset, reset } from "../test-utils/database";
import { parseJSONResponse } from "../test-utils/http";
import { LOGIN_ROUTE, SERVER_URL, USER_CREATE_ROUTE, WORKSPACE_ROUTE } from "../test-utils/server";

// Add HTTP
chai.use(require("chai-http"));

const USER_MAIL = "testing@repowee.fr";
const USER_PASSWORD = "test";
const WORKSPACE_LIMIT = 1;

let admin_token;
let user_token;
let workspace_service;
let user_service;

describe("WorkspaceAPI", () => {
    before(done => {
        initAndReset()
            .then(() => done())
            .catch(done);
    });

    beforeEach(done => {
        createUserDBService()
            .then(s => {
                user_service = s;

                return chai.request(SERVER_URL)
                    .post(LOGIN_ROUTE)
                    .send({
                        email: UserCollectionService.ADMIN_MAIL,
                        password: "beatsaber"
                    });
            })
            .then(response => {
                chai.assert.equal(response.status, 200);
                admin_token = parseJSONResponse(response).data.token;

                return chai.request(SERVER_URL)
                    .post(USER_CREATE_ROUTE)
                    .set("cookie", "token=" + admin_token)
                    .send({
                        mail: USER_MAIL,
                        password: USER_PASSWORD,
                        role: UserRole.USER,
                        workspacesLimit: WORKSPACE_LIMIT
                    });
            })
            .then(response => {
                chai.assert.equal(response.status, 200);

                return chai.request(SERVER_URL)
                    .post(LOGIN_ROUTE)
                    .send({
                        email: USER_MAIL,
                        password: USER_PASSWORD
                    });
            })
            .then(response => {
                chai.assert.equal(response.status, 200);
                user_token = parseJSONResponse(response).data.token;

                return createWorkspaceDBService();
            })
            .then(s => {
                workspace_service = s;

                done();
            })
            .catch(done);
    });

    after(() => {
        // Close database
        database.close();
    });

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("create", () => {
        it("Should create a workspace", done => {
            const WORKSPACE_NAME = "testing_workspace";

            chai.request(SERVER_URL)
                .post(WORKSPACE_ROUTE(UserCollectionService.ADMIN_MAIL))
                .set("cookie", "token=" + admin_token)
                .send({
                    name: WORKSPACE_NAME
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return workspace_service.all();
                })
                .then(workspaces => {
                    chai.assert.lengthOf(workspaces, 1);
                    chai.assert.equal(workspaces[0].name, WORKSPACE_NAME);

                    return user_service.findByKey(workspaces[0].owner);
                })
                .then(user => {
                    chai.assert.instanceOf(user, User);
                    chai.assert.equal(user.mail, UserCollectionService.ADMIN_MAIL);

                    done();
                })
                .catch(done);
        });

        it("Should throw a workspace limit error", done => {
            chai.request(SERVER_URL)
                .post(WORKSPACE_ROUTE(USER_MAIL))
                .set("cookie", "token=" + user_token)
                .send({
                    name: "testing_workspace"
                })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .post(WORKSPACE_ROUTE(USER_MAIL))
                        .set("cookie", "token=" + user_token)
                        .send({
                            name: "testing_workspace_2"
                        });
                })
                .then(response => {
                    chai.assert.notEqual(response.status, 200);
                    chai.assert.equal(parseJSONResponse(response).error, WORKSPACE_API.LIMIT_REACHED);

                    done();
                })
                .catch(done);
        });
    });
});