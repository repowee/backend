import chai from "chai";
import { after, afterEach, before, beforeEach, describe, it } from "mocha";
import io from "socket.io-client";
import { AUTH } from "../../src/common/errors";
import { database } from "../../src/config/database";
import { create, UserCollectionService } from "../../src/services/db/user";
import { CONNECTION_MESSAGE } from "../../src/websocket/ws";
import { initAndReset, reset } from "../test-utils/database";
import { parseJSONResponse } from "../test-utils/http";
import { LOGIN_ROUTE, SERVER_URL } from "../test-utils/server";

// Add HTTP
chai.use(require("chai-http"));

let admin_token;

describe("Websocket", () => {

    before(done => {
        initAndReset()
            .then(() => done())
            .catch(done);
    });

    beforeEach(done => {
        create()
            .then(() => {
                return chai.request(SERVER_URL)
                    .post(LOGIN_ROUTE)
                    .send({
                        email: UserCollectionService.ADMIN_MAIL,
                        password: "beatsaber"
                    });
            })
            .then(response => {
                chai.assert.equal(response.status, 200);
                admin_token = parseJSONResponse(response).data.token;

                done();
            })
            .catch(done);
    });

    after(() => {
        // Close database
        database.close();
    });

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("Authentication", () => {
        it("Should throw an auth error", done => {
            const socket = io(SERVER_URL, { path: "/ws", query: { token: "kesdfhjvbjksdvbfs" } });

            socket.on("error", data => {
                chai.assert.equal(data, "jwt malformed");

                socket.close();
                done();
            });
            socket.on("connection", data => done(data));
        });

        it("Should throw a token error", done => {
            const socket = io(SERVER_URL, { path: "/ws" });

            socket.on("error", data => {
                chai.assert.equal(data, AUTH.TOKEN_UNDEFINED);

                socket.close();
                done();
            });
            socket.on("connection", data => done(data));
        });

        it("Should connect successfully", done => {
            const socket = io(SERVER_URL, { path: "/ws", query: { token: admin_token } });

            socket.on("error", data => done(data));
            socket.on("connection", data => {
                chai.assert.equal(data, CONNECTION_MESSAGE);

                socket.close();
                done();
            });
        });
    });
});