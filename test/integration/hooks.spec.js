import { after, before } from "mocha";
import server from "../../src";
import { JOBS } from "../../src/services/scheduler";

// Wait server start
before(done => {
    server.on("ready", () => done());
});

// Kill server after all tests & stop cron jobs
after(() => {
    server.close();
    JOBS.forEach(j => j.stop());
});