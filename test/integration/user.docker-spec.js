import chai from "chai";
import { after, afterEach, before, beforeEach, describe, it } from "mocha";
import { USER_API } from "../../src/common/errors";
import { database } from "../../src/config/database";
import { URL_SELF_KEYWORD } from "../../src/router/route";
import { create as createUserDBService, UserCollectionService } from "../../src/services/db/user";
import { create as createWorkspaceDBService } from "../../src/services/db/workspace";
import { initAndReset, reset } from "../test-utils/database";
import { parseJSONResponse } from "../test-utils/http";
import { LOGIN_ROUTE, SERVER_URL, USER_ROUTE, WORKSPACE_ROUTE } from "../test-utils/server";

// Add HTTP
chai.use(require("chai-http"));

let admin_token;

describe("UserAPI", () => {
    before(done => {
        initAndReset()
            .then(() => done())
            .catch(done);
    });

    beforeEach(done => {
        createUserDBService()
            .then(() => {
                return chai.request(SERVER_URL)
                    .post(LOGIN_ROUTE)
                    .send({
                        email: UserCollectionService.ADMIN_MAIL,
                        password: "beatsaber"
                    });
            })
            .then(response => {
                chai.assert.equal(response.status, 200);
                admin_token = parseJSONResponse(response).data.token;

                return createWorkspaceDBService();
            })
            .then(() => done())
            .catch(done);
    });

    after(() => {
        // Close database
        database.close();
    });

    afterEach(done => {
        reset().then(() => done()).catch(done);
    });

    describe("delete", () => {
        it("Should throw a workspace error", done => {
            chai.request(SERVER_URL)
                .post(WORKSPACE_ROUTE(URL_SELF_KEYWORD))
                .set("cookie", "token=" + admin_token)
                .send({ name: "testing_network" })
                .then(response => {
                    chai.assert.equal(response.status, 200);

                    return chai.request(SERVER_URL)
                        .delete(USER_ROUTE + URL_SELF_KEYWORD)
                        .set("cookie", "token=" + admin_token);
                })
                .then(response => {
                    chai.assert.notEqual(response.status, 200);
                    chai.assert.equal(parseJSONResponse(response).error, USER_API.WORKSPACE_OWNER);

                    done();
                })
                .catch(done);
        });
    });
});