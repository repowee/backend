import { assert } from "chai";
import { after, before, describe, it } from "mocha";
import { database } from "../../src/config/database";
import { collection, collectionExists } from "../../src/utils/database";
import { initAndReset } from "../test-utils/database";

describe("Database utilities", () => {
    before((done) => {
        initAndReset().then(done).catch(done);
    });

    after(() => database.close());

    describe("#collectionExists", () => {

        it("Collection shouldn't exist", done => {
            collectionExists("NotACollection")
                .then((exist) => {
                    assert.isFalse(exist);
                    done();
                }).catch(done);
        });

        it("Collection should exist", done => {
            const COLLECTION_NAME = "test-collection";

            database.collection(COLLECTION_NAME).create()
                .then(() => collectionExists(COLLECTION_NAME))
                .then((exist) => {
                    assert.isTrue(exist);
                    done();
                }).catch(done);
        });
    });

    describe("#collection", () => {
        it("Should get/create collection", done => {
            collection("test-create-collection")
                .then(col => col.drop())
                .then(() => done())
                .catch(done);
        });

        it("Should get collection", done => {
            const COLLECTION_NAME = "test-get-collection";

            database.collection(COLLECTION_NAME).create()
                .then(() => collection(COLLECTION_NAME))
                .then(col => col.drop())
                .then(() => done())
                .catch(done);
        });
    });
});