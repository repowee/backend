import { assert } from "chai";
import { describe, it } from "mocha";
import ValidationError from "../../src/model/error";
import { Workspace } from "../../src/model/workspace";
import { UserCollectionService } from "../../src/services/db/user";
import { VALID_WORKSPACE } from "../common/workspace";

describe("Workspace", () => {

    describe("#Constructor", () => {
        it("Should filled the object", () => {
            const obj = {
                name: "vhvqv",
                owner: "djvnblv",
                network: "delkv bv"
            };

            const workspace = new Workspace(obj.name, obj.owner, obj.network);

            assert.equal(workspace.name, obj.name);
            assert.equal(workspace.owner, obj.owner);
            assert.equal(workspace.network, obj.network);
        });

        it("Should create a valid object", () => {
            Workspace.Check(new Workspace("ejv bq", UserCollectionService.COLLECTION_NAME + "/vefbqs", "cvbvsdq"));
        });

        it("Should throw an error", () => {
            // Name
            assert.throws(() => Workspace.Check(new Workspace(), ValidationError));
            assert.throws(() => Workspace.Check(new Workspace({}), ValidationError));

            // Owner
            assert.throws(() => Workspace.Check(new Workspace("fedvvv"), ValidationError));
            assert.throws(() => Workspace.Check(new Workspace("rebkskjndfb", {}), ValidationError));
            assert.throws(() => Workspace.Check(new Workspace("rebkskjndfb", "sgcfvcvsqc"), ValidationError));

            // Network
            assert.throws(() => Workspace.Check(new Workspace("s!ddfbjv", UserCollectionService.COLLECTION_NAME + "/hvbv"), ValidationError));
            assert.throws(() => Workspace.Check(new Workspace("vevqsvcqs", UserCollectionService.COLLECTION_NAME + "/dsvqvdv", {}), ValidationError));
        });
    });

    describe("#Check", () => {
        it("Should throw an error", () => {
            assert.throws(() => Workspace.Check({}), ValidationError);
            assert.throws(() => Workspace.Check(), ValidationError);
        });

        it("Should validate a partial object", () => {
            Workspace.Check(new Workspace("devbq"), false);
            Workspace.Check(new Workspace(undefined, UserCollectionService.COLLECTION_NAME + "/devbq"), false);
            Workspace.Check(new Workspace(undefined, undefined, "devbq"), false);
        });
    });

    describe("#toJSON", () => {
        it("JSON.stringify should use #toJSON", () => {
            assert.equal(JSON.stringify(VALID_WORKSPACE), JSON.stringify(VALID_WORKSPACE.toJSON()));
        });
    });

    describe("#FromObject", () => {
        it("Should create a valid user from an object", () => {
            assert.deepEqual(Workspace.FromObject(JSON.parse(JSON.stringify(VALID_WORKSPACE))).toJSON(), VALID_WORKSPACE.toJSON());
            assert.deepEqual(Workspace.FromObject(undefined), new Workspace());
            assert.deepEqual(Workspace.FromObject({}), new Workspace());
        });
    });
});