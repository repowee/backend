import { assert } from "chai";
import { describe, it } from "mocha";
import { application as appConfig } from "../../src/config/config";
import ValidationError from "../../src/model/error";
import { AuthInfo, User, UserRole } from "../../src/model/user";
import { VALID_AUTH, VALID_USER } from "../common/user";

describe("User", () => {
    describe("#Constructor", () => {
        it("Should filled object", () => {
            const obj = {
                name: "Mr Test",
                mail: "test@test.fr",
                password: "skjfnbgvsvbnskfhjbvdsvjh",
                role: UserRole.USER,
                auth: {
                    token: "efvdbdjsfhvbsv"
                },
                workspacesLimit: 10
            };

            const user = new User(obj.name, obj.mail, obj.password, obj.role, new AuthInfo(obj.auth.token), obj.workspacesLimit);

            assert.equal(user.name, obj.name);
            assert.equal(user.mail, obj.mail);
            assert.equal(user.password, obj.password);
            assert.equal(user.role, obj.role);
            assert.equal(user.auth.token, obj.auth.token);
            assert.equal(user.workspacesLimit, obj.workspacesLimit);
        });

        it("Should create a valid object", () => {
            User.Check(new User(undefined, "test@test.fr", "sdvfedbsbhsthdxbdxb", UserRole.USER, new AuthInfo("ffvbhhdvb"), 0));
            User.Check(new User(undefined, "test@test.fr", "sdvfedbsbhsthdxbdxb", UserRole.USER, new AuthInfo("ffvbhhdvb"), 10));
            User.Check(new User(undefined, "test@test.fr", "sdvfedbsbhsthdxbdxb", UserRole.USER, new AuthInfo("ffvbhhdvb")));
            User.Check(new User("undefined", "test@test.fr", "sdvfedbsbhsthdxbdxb", UserRole.USER));
        });

        it("Should throw an error", () => {
            // Name
            assert.throws(() => User.Check(new User({}), ValidationError));

            // Email
            assert.throws(() => User.Check(new User(undefined), ValidationError));
            assert.throws(() => User.Check(new User(undefined, {}), ValidationError));
            assert.throws(() => User.Check(new User(undefined, "vjkfvbqdv"), ValidationError));

            // Password
            assert.throws(() => User.Check(new User(undefined, "test@test.fr"), ValidationError));
            assert.throws(() => User.Check(new User(undefined, "test@test.fr", {}), ValidationError));

            // Role
            assert.throws(() => User.Check(new User(undefined, "test@test.fr", "vbnivvbsd", "NOT_A_ROLE"), ValidationError));
            assert.throws(() => User.Check(new User(undefined, "test@test.fr", "vbnivvbsd"), ValidationError));

            // Auth info
            assert.throws(() => User.Check(new User(undefined, "vjkfvbqdv", "jfhvbvbqv", UserRole.USER, {}), ValidationError));

            // Workspaces
            assert.throws(() => User.Check(new User(undefined, "test@test.fr", "skfbvs", UserRole.USER, new AuthInfo("skfdbb "), {}), ValidationError));
            assert.throws(() => User.Check(new User(undefined, "test@test.fr", "skfbvs", UserRole.USER, new AuthInfo("skfdbb "), -10), ValidationError));
        });

        it("Should have the default workspaces limit", () => {
            assert.equal(new User().workspacesLimit, appConfig.workspacesLimit);
        });
    });

    describe("#Check", () => {
        it("Should throw an error", () => {
            assert.throws(() => User.Check({}), ValidationError);
            assert.throws(() => User.Check(), ValidationError);
        });

        it("Should validate partial object", () => {
            User.Check(new User("Mr test"), false);
            User.Check(new User(undefined, "test@test.fr"), false);
            User.Check(new User(undefined, undefined, "gevjbfdvksv"), false);
            User.Check(new User(undefined, undefined, undefined, UserRole.ADMIN), false);
            User.Check(new User(undefined, undefined, undefined, undefined, new AuthInfo()), false);
            User.Check(new User(undefined, undefined, undefined, undefined, undefined, 10), false);
        });
    });

    describe("#toJSON", () => {
        it("JSON.stringify should use #toJSON", () => {
            assert.equal(JSON.stringify(VALID_USER), JSON.stringify(VALID_USER.toJSON()));
        });
    });

    describe("#FromObject", () => {
        it("Should create a valid user from an object", () => {
            assert.deepEqual(User.FromObject(JSON.parse(JSON.stringify(VALID_USER))).toJSON(), VALID_USER.toJSON());
            assert.deepEqual(User.FromObject(undefined), new User());
            assert.deepEqual(User.FromObject({}), new User());
        });
    });
});

describe("AuthInfo", () => {
    describe("#Constructor", () => {
        it("Should filled object", () => {
            const obj = {
                token: "zvbqvbv"
            };

            const info = new AuthInfo(obj.token);

            assert.equal(info.token, obj.token);
        });

        it("Should create a valid object", () => {
            AuthInfo.Check(new AuthInfo("effvbhdbsfdb"));
        });
    });

    describe("#toJSON", () => {
        it("JSON.stringify should use #toJSON", () => {
            assert.equal(JSON.stringify(VALID_AUTH), JSON.stringify(VALID_AUTH.toJSON()));
        });
    });

    describe("#FromObject", () => {
        it("Should create a valid user from an object", () => {
            assert.deepEqual(AuthInfo.FromObject(JSON.parse(JSON.stringify(VALID_AUTH))).toJSON(), VALID_AUTH.toJSON());
            assert.deepEqual(AuthInfo.FromObject(undefined), new AuthInfo());
            assert.deepEqual(AuthInfo.FromObject({}), new AuthInfo());
        });
    });

    describe("#Check", () => {
        it("Should validate partial object", () => {
            AuthInfo.Check(new AuthInfo(undefined), false);
            AuthInfo.Check(new AuthInfo("ehvbvbqsvc"), false);
        });

        it("Should throw an error", () => {
            assert.throws(() => AuthInfo.Check(new AuthInfo({})), ValidationError);
            assert.throws(() => AuthInfo.Check(""), ValidationError);
        });
    });
});

describe("UserRole", () => {
    describe("#Check", () => {
        it("Should validate declared variables", () => {
            for (let role of [ UserRole.USER, UserRole.ADMIN ]) {
                UserRole.Check(role);
            }
        });

        it("Should throw an error", () => {
            assert.throws(() => UserRole.Check(undefined), ValidationError);
            assert.throws(() => UserRole.Check(""), ValidationError);
        });
    });
});