import { DockerService } from "../../../src/services/docker/docker";
import { LOGGER } from "../../../src/utils";
const fs = require("fs");

/**
 * Docker service exemple of use
 */

export function dockerExemple() {

    LOGGER.info("Docker Exemple");

    var monServiceDocker = new DockerService();

    monServiceDocker.buildImageFromFolder(__dirname + "/projectTest/test1/").then((imageName) => {

        LOGGER.info("My project builded ! My image name : " + imageName);

        var myContainerOutpus = fs.createWriteStream(__dirname + "/projectTest/containerOutput");

        monServiceDocker.runContainer(imageName, myContainerOutpus).then((data) => {
            // The container terminated
            LOGGER.info("My container is finished with status code " + data[0].StatusCode);
        }).catch(e => {
            // A problem during the container creation
            LOGGER.info("My container failed to run");
            LOGGER.error(e);
        });

        LOGGER.info("My container is running");

        monServiceDocker.getContainers().then((cont) => {
            LOGGER.info("nb cont : " + cont.length);
        });

        setTimeout(() => {
            monServiceDocker.getContainers().then((cont) => {
                LOGGER.info("nb cont : " + cont.length);
            });
        }, 1500);

    }).catch((err) => {
        LOGGER.info("My project build failed... ");
        LOGGER.info(err);
        console.log(err);

    });


    monServiceDocker.getImages().then((imgs) => {
        LOGGER.info("nb Images : " + imgs.length);
    });

    monServiceDocker.getContainers().then((cont) => {
        LOGGER.info("nb cont : " + cont.length);
    });
}
