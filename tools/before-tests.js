import { Database } from "arangojs";
import { config } from "../src/config/database";
import { LOGGER } from "../src/utils";

const run = async () => {
    const database = new Database(config.options);

    // Connect to database
    await database.login(config.auth.user, config.auth.password);

    // List databases
    const databases = await database.listDatabases();

    // Create test database
    if (databases.indexOf(config.name) !== -1) {
        return;
    }
    await database.createDatabase(config.name, [ { username: "root" } ]);
    LOGGER.info("Create " + config.name + " database");
};

// Run
run().catch(e => {
    LOGGER.error(e);

    process.exit(1);
});