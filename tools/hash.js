import { genSalt } from "bcrypt";
import { hash, SALT_ROUNDS } from "../src/utils/hash";

const args = process.argv.slice(2);

// Check arguments
if (args.length !== 1) {
    console.log("Usage: npm run hash [password]");
    process.exit(1);
}

// Generate salt
genSalt(SALT_ROUNDS)
    .then(salt => {
        console.log("Salt: " + salt);
        return salt;
    }).then(salt => hash(args[0], salt))
    .then(password => console.log("Password: " + password));
