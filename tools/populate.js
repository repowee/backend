import axios from "axios";
import faker from "faker";
import { UserRole } from "../src/model/user";
import { UserCollectionService } from "../src/services/db/user";
import LOGGER from "../src/utils/logger";
import { LOGIN_ROUTE, SERVER_URL, USER_CREATE_ROUTE, USER_LIST_ROUTE, WORKSPACE_ROUTE } from "../test/test-utils/server";

const args = process.argv.slice(2);

// Check arguments
if (args.length !== 1) {
    console.log("Usage: npm run populate [people_count]");
    process.exit(1);
}

// Check argument
const PEOPLE_COUNT = parseInt(args[0]);
if (isNaN(PEOPLE_COUNT)) {
    console.error("Invalid people_count");
    process.exit(1);
}

axios.post(SERVER_URL + LOGIN_ROUTE, {
    email: UserCollectionService.ADMIN_MAIL,
    password: "beatsaber"
})
    .then(async response => {
        // Create an instance
        const admin_instance = axios.create({
            baseURL: SERVER_URL,
            headers: {
                Cookie: "token=" + response.data.data.token
            }
        });

        // Create users
        for (let i = 0; i < PEOPLE_COUNT; i++) {
            const card = faker.helpers.userCard();

            await admin_instance.post(USER_CREATE_ROUTE, {
                name: card.name,
                mail: card.email,
                password: "faker",
                role: UserRole.USER
            });
        }
        LOGGER.info(`Create ${PEOPLE_COUNT} users`);

        // Get all users
        const users = (await admin_instance.get(USER_LIST_ROUTE)).data.data;

        // Create workspaces
        const workspaces = [];
        for (let i = 0; i < PEOPLE_COUNT / 4; i++) {
            for (let j = 0; j < 2; j++) {
                const name = faker.random.words();
                workspaces.push({ name: name, user: users[i].mail });

                await admin_instance.post(WORKSPACE_ROUTE(users[i].mail), {
                    name: name
                });
            }
        }
        LOGGER.info(`Create ${PEOPLE_COUNT} workspaces`);

        // Invite people
        for (let workspace of workspaces) {
            for (let i = PEOPLE_COUNT - 1; i >= 0; i--) {
                if (workspace.user === users[i].mail) {
                    continue;
                }

                await admin_instance.post(WORKSPACE_ROUTE(workspace.user) + "/" + workspace.name + "/invite", {
                    user: users[i].mail
                });
            }
        }
    })
    .catch(e => LOGGER.error(e));